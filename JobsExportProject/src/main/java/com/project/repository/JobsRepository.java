package com.project.repository;

import java.util.List;

import javax.xml.bind.Marshaller;

import com.opencsv.CSVWriter;
import com.project.model.JobsModel;
import com.project.model.Url;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.sun.jersey.api.client.WebResource;
import com.sun.xml.bind.marshaller.DataWriter;

public interface JobsRepository {

	DataWriter writeStartElement(ExportSourcesModel exportSourcesModel);

	List<JobsModel> getJobsFromDbWithStartId(String query, int start, int limit);

	Marshaller createMarshaller(String source);

	String cutCpcStringToHalf(String cpc, JobsModel model);

	String callUrlShortnerAPI(String url, WebResource webResource);

	String encryptUrl(Url url, String urlDomainParams, String urlEndParams,String aIdParamName);

	String updateSubIdBySourceInUrl(String source, String url, List<SubIdModel> subIdLIst);

	CSVWriter writeCsvFileInLocal(ExportSourcesModel exportSourcesModel);

	int getinitialCountFromDb(String query);

}
