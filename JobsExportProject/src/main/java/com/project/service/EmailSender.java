package com.project.service;

import java.util.List;

import com.project.model.EmailReceiver;
import com.project.model.json.ExportSourcesModel;
import com.project.model.stats.StatsOuterModel;

public interface EmailSender {

	public String sendSmtpEmail(String subject, String body, EmailReceiver emailReceiver, List<ExportSourcesModel> list);

	String createEmailBody(StatsOuterModel statsOuterModel, String sourceName);
}
