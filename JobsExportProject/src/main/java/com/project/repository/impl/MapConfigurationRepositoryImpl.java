package com.project.repository.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.project.application.Application;
import com.project.model.state;
import com.project.model.json.ExportSourceModelDB;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.project.repository.MapConfigurationRepository;

@Repository
public class MapConfigurationRepositoryImpl implements MapConfigurationRepository {

	@Value("${export.sources.json.file.path}")
	String exportSourcesPath;


	@Value("${source.subid.file.path}")
	String subIdModelJasonFilePath;

	@Autowired
	SessionFactory sessionFactory;

	public Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<ExportSourcesModel> getExportSourcesList() {

		Type type = new TypeToken<List<ExportSourcesModel>>() {
		}.getType();

		List<ExportSourcesModel> exportModelList = new ArrayList<ExportSourcesModel>();

		try {
			

			BufferedReader bufferedReader = new BufferedReader(new FileReader(exportSourcesPath));
			exportModelList = new Gson().fromJson(bufferedReader, type);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return exportModelList;
	}

	@Override
	public List<SubIdModel> getSubIdModelList() {

		List<SubIdModel> subIdList = new ArrayList<SubIdModel>();
		try {
			FileReader fileReader = new FileReader(subIdModelJasonFilePath);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			Type type = new TypeToken<List<SubIdModel>>() {
			}.getType();

			subIdList = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, type);

			return subIdList;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Transactional
	@Override
	public List<ExportSourceModelDB> getExportSourcesListFromDataBase() {
		// TODO Auto-generated method stub

		Query sqlQuery = getSessionFactory().createQuery("Select * from tbl_job_export_detail")
				.setResultTransformer(Transformers.aliasToBean(ExportSourceModelDB.class));
		@SuppressWarnings("unchecked")
		List<ExportSourceModelDB> exportSourceModelDBList = sqlQuery.list();

		return exportSourceModelDBList;
	}

	@Transactional
	@Override
	public HashMap<String, String> getStateAndItsAbbrevationMap() {
		// TODO Auto-generated method stub

		Query sqlQuery = getSessionFactory().createSQLQuery("Select abbreviation,name,country from state")
				.setResultTransformer(Transformers.aliasToBean(state.class));
		@SuppressWarnings("unchecked")
		List<state> stateModelList = sqlQuery.list();

		HashMap<String, String> stateModelListMap = new HashMap<String, String>();

		for (state state : stateModelList) {

			if (state.getAbbreviation() != null && !state.getAbbreviation().toLowerCase().contains("null")) {

				stateModelListMap.put(state.getAbbreviation(), state.getName());
			}

		}

		return stateModelListMap;
	}

}
