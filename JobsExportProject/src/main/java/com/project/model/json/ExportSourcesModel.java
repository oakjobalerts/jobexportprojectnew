package com.project.model.json;

import java.util.List;

public class ExportSourcesModel {

	String name;
	String a_id;
	String fileName;

	String xmlStartEelementName;
	String xmlPublisherElementName;
	String xmlPublisherurlElementName;
	String xmlLastBuildDateElementName;

	String url;
	String query;

	int jobExportLimit;
	boolean isActive;
	boolean cutCpcInHalf;

	List<String> includedTitles;
	List<String> excludedTitles;
	List<String> excludedSources;
	List<String> includedSources;
	List<String> multiTitles;
	public String initialCountquery = "";
	public String getInitialCountquery() {
		return initialCountquery;
	}

	public void setInitialCountquery(String initialCountquery) {
		this.initialCountquery = initialCountquery;
	}

	public int getTotalCountOfJobsForExport() {
		return totalCountOfJobsForExport;
	}

	public String getInitialContquery() {
		return initialCountquery;
	}

	
	public int totalCountOfJobsForExport = 0;

	

	public void setTotalCountOfJobsForExport(int totalCountOfJobsForExport) {
		this.totalCountOfJobsForExport = totalCountOfJobsForExport;
	}

	public List<String> getMultiTitles() {
		return multiTitles;
	}

	public void setMultiTitles(List<String> multiTitles) {
		this.multiTitles = multiTitles;
	}

	String minCpc;

	String ftp_user;
	String ftp_pass;

	public String getFtp_user() {
		return ftp_user;
	}

	public void setFtp_user(String ftp_user) {
		this.ftp_user = ftp_user;
	}

	public String getFtp_pass() {
		return ftp_pass;
	}

	public void setFtp_pass(String ftp_pass) {
		this.ftp_pass = ftp_pass;
	}

	public String getMinCpc() {
		return minCpc;
	}

	public void setMinCpc(String minCpc) {
		this.minCpc = minCpc;
	}

	public List<String> getIncludedSources() {
		return includedSources;
	}

	public void setIncludedSources(List<String> includedSources) {
		this.includedSources = includedSources;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getXmlStartEelementName() {
		return xmlStartEelementName;
	}

	public String getXmlPublisherElementName() {
		return xmlPublisherElementName;
	}

	public String getXmlPublisherurlElementName() {
		return xmlPublisherurlElementName;
	}

	public String getXmlLastBuildDateElementName() {
		return xmlLastBuildDateElementName;
	}

	public void setXmlStartEelementName(String xmlStartEelementName) {
		this.xmlStartEelementName = xmlStartEelementName;
	}

	public void setXmlPublisherElementName(String xmlPublisherElementName) {
		this.xmlPublisherElementName = xmlPublisherElementName;
	}

	public void setXmlPublisherurlElementName(String xmlPublisherurlElementName) {
		this.xmlPublisherurlElementName = xmlPublisherurlElementName;
	}

	public void setXmlLastBuildDateElementName(
			String xmlLastBuildDateElementName) {
		this.xmlLastBuildDateElementName = xmlLastBuildDateElementName;
	}

	public String getName() {
		return name;
	}

	public String getA_id() {
		return a_id;
	}

	public String getFileName() {
		return fileName;
	}

	public int getJobExportLimit() {
		return jobExportLimit;
	}

	public boolean isActive() {
		return isActive;
	}

	public boolean isCutCpcInHalf() {
		return cutCpcInHalf;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setA_id(String a_id) {
		this.a_id = a_id;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setJobExportLimit(int jobExportLimit) {
		this.jobExportLimit = jobExportLimit;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setCutCpcInHalf(boolean cutCpcInHalf) {
		this.cutCpcInHalf = cutCpcInHalf;
	}

	public ExportSourcesModel() {
		super();
	}

	public List<String> getIncludedTitles() {
		return includedTitles;
	}

	public void setIncludedTitles(List<String> includedTitles) {
		this.includedTitles = includedTitles;
	}

	public List<String> getExcludedTitles() {
		return excludedTitles;
	}

	public void setExcludedTitles(List<String> excludedTitles) {
		this.excludedTitles = excludedTitles;
	}

	public List<String> getExcludedSources() {
		return excludedSources;
	}

	public void setExcludedSources(List<String> excludedSources) {
		this.excludedSources = excludedSources;
	}

	// "name": "juju",
	// "a_id": "&a_id=Juju",
	// "fileName": "fbg-juju.xml",
	// "isActive": true,
	// "jobExportLimit": 0,
	// "cutCpcInHalf": true
}
