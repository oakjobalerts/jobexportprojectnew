package com.project.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.model.JobsModel;
import com.project.model.json.ExportSourcesModel;
import com.project.model.stats.DateWiseStatsModel;
import com.project.model.stats.StatsOuterModel;
import com.project.service.StatsService;
import com.project.utility.ExportUtilities;
import com.project.utility.Utility;

@Service
public class StatsServiceImpl implements StatsService {

    @Value("${export.count.stats.folder.path}")
    String statsFolderPath;

    @Autowired
    Utility utility;

    @Value("${abort.map.jason.file.path}")
    String abortMapJasonFilePath;

    @Override
    public String getCurrentStatsFullFilePath(String source) {

        String path = statsFolderPath + File.separator + utility.getCurretYear() + File.separator + source;
        String fileName = utility.getCurretMonth() + "-" + utility.getCurretYear() + "-" + source + "-" + "stats.json";

        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }

        String fullFilePathString = path + File.separator + fileName;

        File fullFilePath = new File(fullFilePathString);

        if (!fullFilePath.exists()) {
            try {
                fullFilePath.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return fullFilePathString;
    }

    @Override
    public StatsOuterModel getStatsModel(String path) {

        StatsOuterModel statsOuterModel = new StatsOuterModel();
        try {
            FileReader fileReader = new FileReader(path + "");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            statsOuterModel = new GsonBuilder().setPrettyPrinting().create().fromJson(bufferedReader, StatsOuterModel.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (statsOuterModel == null) {
            statsOuterModel = createEmptyOuterStatsMOdel();
            // writeStatsModelInFile(path, statsOuterModel);
        }

        return statsOuterModel;
    }

    @Override
    public void writeStatsModelInFile(String exportSource, StatsOuterModel statsOuterModel) {

        String filePath = getCurrentStatsFullFilePath(exportSource);

        String statsJson = new GsonBuilder().serializeNulls().setPrettyPrinting().create().toJson(statsOuterModel);

        try {
            OutputStreamWriter outputStreamWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(statsJson);
            bufferedWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public StatsOuterModel createEmptyOuterStatsMOdel() {

        StatsOuterModel statsOuterModel = new StatsOuterModel();

        statsOuterModel.setTotalSent(0);
        statsOuterModel.setDate(utility.getCurretMonth() + "-" + utility.getCurretYear());

        statsOuterModel.setDateWiseTotalSent(new HashMap<String, Integer>());
        statsOuterModel.setCpcWiseTotalSent(new HashMap<String, Integer>());

        DateWiseStatsModel statsModel = new DateWiseStatsModel();
        // statsModel.setDate(utility.getTodayDate());
        statsModel.setSourceWiseDailySentMap(new HashMap<String, Map<String, Integer>>());

        statsOuterModel.setDateWiseDailySentMap(new HashMap<String, DateWiseStatsModel>());

        // Map<String, SourceWiseStatsModel> sourceAndCpcWiseSentMap = new HashMap<String, SourceWiseStatsModel>();

        System.out.println(new Gson().toJson(statsOuterModel));

        return statsOuterModel;
    }

    @Override
    public synchronized StatsOuterModel updateStatsModel(String exportSource, Map<String, Map<String, Integer>> sourceWiseDailySent) {

        String path = getCurrentStatsFullFilePath(exportSource);

        String currentDate = utility.getTodayDate();

        StatsOuterModel statsOuterModel = getStatsModel(path);

        // Update total jobs sent of this month
        Integer total = statsOuterModel.getTotalSent();
        if (total == null) {
            total = 0;
        }
        total = total + ExportUtilities.totalJobsSent.get();
        statsOuterModel.setTotalSent(total);

        // Add today's total jobs sent
        Map<String, Integer> dateWiseTotalSent = statsOuterModel.getDateWiseTotalSent();
        if (dateWiseTotalSent == null) {
            dateWiseTotalSent = new HashMap<String, Integer>();
        }
        dateWiseTotalSent.put(currentDate, ExportUtilities.totalJobsSent.get());

        // Update cpc wise total monthly sent
        Map<String, Integer> cpcWiseTotalSentFromStatsModel = statsOuterModel.getCpcWiseTotalSent();
        if (cpcWiseTotalSentFromStatsModel == null) {
            cpcWiseTotalSentFromStatsModel = new HashMap<String, Integer>();
        }

        Map<String, Integer> cpcWiseTotalSent = ExportUtilities.cpcWiseTotalSent;
        for (Entry<String, Integer> map : cpcWiseTotalSent.entrySet()) {

            Integer cpcSentCount = cpcWiseTotalSentFromStatsModel.get(map.getKey());
            if (cpcSentCount == null) {
                cpcSentCount = 0;
            }
            Integer updatedCpc = cpcSentCount + map.getValue();
            cpcWiseTotalSentFromStatsModel.put(map.getKey(), updatedCpc);
        }
        statsOuterModel.setCpcWiseTotalSent(cpcWiseTotalSentFromStatsModel);

        // Update date wise daily sent stats
        Map<String, DateWiseStatsModel> dateWiseDailyWiseTotalSentFromObjectModel = statsOuterModel.getDateWiseDailySentMap();

        DateWiseStatsModel sourceWiseStatsModel = new DateWiseStatsModel();
        sourceWiseStatsModel.setSourceWiseDailySentMap(sourceWiseDailySent);

        dateWiseDailyWiseTotalSentFromObjectModel.put(currentDate, sourceWiseStatsModel);
        statsOuterModel.setDateWiseDailySentMap(dateWiseDailyWiseTotalSentFromObjectModel);

        statsOuterModel.setExportSource(exportSource);

        return statsOuterModel;
    }

    @Override
    public synchronized Map<String, Map<String, Integer>> getCpcCountMap(List<JobsModel> jobsListUpdateCpcWithShortnerUrl, Map<String, Map<String, Integer>> dateWiseDailySent) {

        for (JobsModel jobsModel : jobsListUpdateCpcWithShortnerUrl) {

            // Increment Global total sent for this export of this day
            ExportUtilities.totalJobsSent.getAndIncrement();

            Map<String, Integer> cpcCountMap = dateWiseDailySent.get(jobsModel.getSourceName());
            if (cpcCountMap == null) {
                cpcCountMap = new HashMap<String, Integer>();
            }

            // Increment total source wise sent count(like j2c, lead 5 xml)
            Integer total = cpcCountMap.get("total");
            if (total == null) {
                total = 0;
            }
            total = total + 1;
            cpcCountMap.put("total", total);

            // Increment cpc wise sent count per job source
            Integer count = cpcCountMap.get(jobsModel.getCpc());
            if (count == null) {
                count = 0;
            }
            count = count + 1;
            cpcCountMap.put(jobsModel.getCpc(), count);
            dateWiseDailySent.put(jobsModel.getSourceName(), cpcCountMap);

            // Increment global cpc sent count of this day
            Integer totalCpcSent = ExportUtilities.cpcWiseTotalSent.get(jobsModel.getCpc());
            if (totalCpcSent == null) {
                totalCpcSent = 0;
            }
            totalCpcSent = totalCpcSent + 1;
            ExportUtilities.cpcWiseTotalSent.put(jobsModel.getCpc(), totalCpcSent);
        }

        return dateWiseDailySent;
    }

    @Override
    public void resetJobExportAbortMapToFlase(List<ExportSourcesModel> exportSourceModelList) {

        Map<String, String> abortJasonMap = new HashMap<String, String>();
        try {

            for (ExportSourcesModel exportSourcesModel : exportSourceModelList) {
                abortJasonMap.put(exportSourcesModel.getName(), "false");
            }

            File file = new File(abortMapJasonFilePath);

            if (!file.exists()) {
                file.createNewFile();
            }

            OutputStreamWriter outputStreamWriter = new FileWriter(abortMapJasonFilePath);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(abortJasonMap));
            bufferedWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
