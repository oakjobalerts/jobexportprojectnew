package com.project.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Controller;

@Controller
public class Utility {

	public static HashMap<String, String> cpc_more_than_one_point_two_map = new HashMap<String, String>();

	static DateFormat dateFormatter = new SimpleDateFormat("YYYY-MM-dd");
	public static String baseFilePath = "/var/nfs-93/redirect/mis_logs/";

	public String getTodayDate() {

		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatter.format(new Date());
	}

	public String nullCheck(String value) {

		return value == null ? "" : value;
	}

	public String getCurretMonth() {

		DateFormat dateFormatter = new SimpleDateFormat("MMMM");
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatter.format(new Date());

	}

	public String getCurretYear() {

		DateFormat dateFormatter = new SimpleDateFormat("YYYY");
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatter.format(new Date());
	}

	public String getDateMonthINNumber() {

		DateFormat dateFormatter = new SimpleDateFormat("ddMM");
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatter.format(new Date());
	}

	public String getDateByInterval(int interval) {

		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.DATE, interval);

		return dateFormatter.format(calendar.getTime());
	}

	public String getLastMonthYear() {

		DateFormat dateFormatter = new SimpleDateFormat("MMMM-YYYY");

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -1);

		return dateFormatter.format(calendar.getTime());
	}

	public boolean isStartDateSmallerOrEqualsThanEndDate(String startDate, String endDate) {

		try {

			startDate = startDate.split(" ")[0];
			endDate = endDate.split(" ")[0];

			Date start = dateFormatter.parse(startDate);
			Date end = dateFormatter.parse(endDate);

			if (start.getTime() <= end.getTime()) {

				return true;
			}

		} catch (Exception e) {
			System.out.println(e);
			return false;
		}

		return false;
	}

	public List<String> getDateListDescForThisMonth() {

		List<String> dateList = new ArrayList<String>();

		Calendar startDateCalendar = Calendar.getInstance();
		startDateCalendar.set(Calendar.DAY_OF_MONTH, 1);

		Calendar endDateCalendar = Calendar.getInstance();
		endDateCalendar.set(Calendar.DAY_OF_MONTH, startDateCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		while (endDateCalendar.getTime().after(startDateCalendar.getTime())) {

			dateList.add(formatter.format(endDateCalendar.getTime()));
			endDateCalendar.add(Calendar.DATE, -1);
		}
		dateList.add(formatter.format(startDateCalendar.getTime()));

		return dateList;
	}

	public String getFirstDateOfThisMonth() {

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);

		String firstDateOfThisMonth = dateFormatter.format(cal.getTime());
		return firstDateOfThisMonth;
	}

	public static void sendSms(String message) {

		BufferedReader in = null;
		try {

			// if (Main.isTest == true) {
			// phoneNumber = "8860547656";
			// }

			message = URLEncoder.encode(message, "UTF-8");
			String phoneNumber = "";
			String url = "http://bhashsms.com/api/sendmsg.php?user=signity1&pass=signity123&sender=RSTRNT&phone=";
			try {
				BufferedReader bufferedReader = new BufferedReader(new FileReader(baseFilePath + "update_server_count/phonenumberlist.txt"));
				String line = bufferedReader.readLine();
				if (line != null)
					phoneNumber = line;
				bufferedReader.close();
			} catch (Exception e) {
			}

			url = url + phoneNumber;
			String text = "&text=" + message;
			url = url + text;
			url = url + "&priority=ndnd&stype=normal";
			System.out.println("Url=" + url);
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", "Mozilla/5.0");

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				System.out.println("Message Send Successfully");
				System.out.println("\nSending 'GET' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);

				in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
			}
		} catch (Exception e) {
			try {
				in.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

	}

}
