package com.project.model;

public class ExportJobCountModel {
	private Number count;

	public Number getCount() {
		return count;
	}

	public void setCount(Number count) {
		this.count = count;
	}

}
