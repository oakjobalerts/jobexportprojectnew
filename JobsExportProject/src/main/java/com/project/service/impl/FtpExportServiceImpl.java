package com.project.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPOutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.project.model.json.ExportSourcesModel;
import com.project.service.FtpExportService;

@Repository
public class FtpExportServiceImpl implements FtpExportService {

	@Value("${oakjob.ftp.host}")
	String ftpHost;

	@Value("${oakjob.ftp.user}")
	String ftpUser;

	@Value("${oakjob.ftp.password}")
	String ftpPass;

	@Value("${oakjob.ftp.file.upload.folder.path}")
	String ftpFolder;

	@Value("${local.server.folder.path}")
	String localFolderPath;
	// =====================testing

	@Value("${oakjob.ftp.host.test}")
	String ftpHost_test;

	@Value("${oakjob.ftp.user.test}")
	String ftpUser_test;

	@Value("${oakjob.ftp.password.test}")
	String ftpPass_test;

	@Value("${oakjob.ftp.file.upload.folder.path.test}")
	String ftpFolder_test;

	@Value("${local.server.folder.path}")
	String localFolderPath_test;

	@Override
	public String uploadSFTPFile(ExportSourcesModel model) {
		String fileSizeIn = "";
		try {
			JSch jsch = new JSch();

			// means here we are uploading file over the new ftp other than
			// oakjob
			if (!model.getFtp_user().equalsIgnoreCase("root")) {
				System.out.println("it will write in new ftp");
				ftpHost = ftpHost_test;
				ftpUser = ftpUser_test;
				ftpPass = ftpPass_test;
				ftpFolder = ftpFolder_test;
				ftpFolder = ftpFolder + model.getFtp_user() + "/";

			}

			System.out.println(ftpHost + " " + "user=" + ftpUser + " pass=" + ftpPass);
			Session session = jsch.getSession(ftpUser, ftpHost, 22);
			session.setPassword(ftpPass);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			// sftp
			Channel channel = null;
			ChannelSftp channelSftp = null;
			try {
				channel = session.openChannel("sftp");
				channel.connect();
				channelSftp = (ChannelSftp) channel;
				channelSftp.cd(ftpFolder);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			System.out.println("ftpFolder" + ftpFolder);
			try {
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String localFilePath = localFolderPath + model.getFileName();
			String ftpFilePath = ftpFolder + model.getFileName();

//			File localFile = new File(localFilePath);

//			if (model.getName().equalsIgnoreCase("macro") ||
//					model.getName().equalsIgnoreCase("juju")
//					||
//					model.getName().equalsIgnoreCase("dmsjob")
//					||
//					model.getName().equalsIgnoreCase("Jobtome")
//					
//					) {
				System.out.println("going for gz file format");

				String gzFilePath = localFolderPath + "fbg-" + model.getName() + ".gz";
				File localFile = new File(compressGzipFile(localFilePath, gzFilePath));
				ftpFilePath = ftpFolder + "fbg-" + model.getName() + ".gz";

//			}

			double fileSize = localFile.length();
			InputStream inputStream = new FileInputStream(localFile);

			try {
				System.out.println("localFile" + localFile);
				System.out.println("ftpFilePath" + ftpFilePath);
				channelSftp.put(inputStream, ftpFilePath);
				inputStream.close();

				double actual_size = 0;

				if (fileSize > 1024000) {
					actual_size = fileSize / 1024000;
					actual_size = Math.round(actual_size * 100);
					actual_size = actual_size / 100;
					fileSizeIn = "" + actual_size + " MB";
				} else {
					actual_size = fileSize / 1024;
					actual_size = Math.round(actual_size * 100);
					actual_size = actual_size / 100;
					fileSizeIn = "" + actual_size + " KB";

				}
				// getting size of file in mb
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {

				channelSftp.exit();
				session.disconnect();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileSizeIn;

	}

	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}

	private static String compressGzipFile(String xmlFile, String gzipFile) {
		try {
			FileInputStream fis = new FileInputStream(xmlFile);
			FileOutputStream fos = new FileOutputStream(gzipFile);
			GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
			byte[] buffer = new byte[1024];
			int len;
			while ((len = fis.read(buffer)) != -1) {
				gzipOS.write(buffer, 0, len);
			}
			// close resources
			gzipOS.close();
			fos.close();
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("gzipFile" + gzipFile);

		return gzipFile;

	}
}

// @Override
// public void uploadFileOverFtpServer() {
//
// FTPSClient ftpClient = new FTPSClient();
//
// String server = ftpHost;
// int port = 22;
// String user = ftpUser;
// String pass = ftpPass;
//
// try {
//
// ftpClient.connect(server, port);
// ftpClient.login(user, pass);
// ftpClient.enterLocalPassiveMode();
//
// File localFile = new File(folderPath + fileName);
//
// String firstRemoteFile = fileName;
//
// ftpClient.changeWorkingDirectory(ftpFolder);
// int replyCode = ftpClient.getReplyCode();
//
// if (replyCode != 250) {
// ftpClient.makeDirectory(ftpFolder);
// }
//
// InputStream inputStream = new FileInputStream(localFile);
//
// boolean done = ftpClient.storeFile(firstRemoteFile, inputStream);
// inputStream.close();
// if (done) {
// System.out.println("The first file is uploaded successfully.");
// }
//
// inputStream.close();
//
// if (ftpClient.isConnected()) {
// ftpClient.logout();
// ftpClient.disconnect();
// }
//
// } catch (Exception e) {
// e.printStackTrace();
// }
//
// }
