package com.project.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "job")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExportMacroModel {

	// TODO IMPORTANT : change setters methods to include cdata format

	String jobId;

	String title;
	String date;
	String url;
	String city;
	String state;
	String country;
	String description;
	String location;
	String company;

	String cpc;

	String source;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = "<![CDATA[" + source + " ]]>";
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = "<![CDATA[" + jobId + " ]]>";
	}

	public String getCpc() {
		return cpc;
	}

	public void setCpc(String cpc) {
		this.cpc = "<![CDATA[" + cpc + " ]]>";
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = "<![CDATA[" + company + " ]]>";
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = "<![CDATA[" + location + " ]]>";
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = "<![CDATA[" + city + " ]]>";
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = "<![CDATA[" + state + " ]]>";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = "<![CDATA[" + title + " ]]>";
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = "<![CDATA[" + date + " ]]>";
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = "<![CDATA[" + url + " ]]>";
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = "<![CDATA[" + country + " ]]>";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = "<![CDATA[" + description + " ]]>";
	}

}
