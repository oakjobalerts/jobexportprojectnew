package com.project.model.json;

public class SubIdModel {

    String source;
    String removeSubString;
    String subId;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getRemoveSubString() {
        return removeSubString;
    }

    public String getSubId() {
        return subId;
    }

    public void setRemoveSubString(String removeSubString) {
        this.removeSubString = removeSubString;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public SubIdModel() {
        super();
    }

}
