package com.project.utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class ExportUtilities {
	public static AtomicInteger numberOfJobsProcessed = new AtomicInteger(0);

	public static CountDownLatch latch;

	public static AtomicInteger numberOfThreadCompleted = new AtomicInteger(0);

	public static AtomicInteger resultCounter = new AtomicInteger(0);

	public static DateFormat uniqueIdDateFormatter = new SimpleDateFormat("yyyyMMdd");
	public static DateFormat uniqueIdDateFormatter_for_macro = new SimpleDateFormat("SSSmmMMyyyyHHssdd");

	public static DateFormat todayDateFormatterForSentDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	public static volatile Map<String, Map<String, Integer>> dateWiseDailySent;

	public static volatile AtomicInteger totalJobsSent = new AtomicInteger(0);

	public static volatile Map<String, Integer> cpcWiseTotalSent;

}
