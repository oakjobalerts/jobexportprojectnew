package com.project.model;

import java.util.List;

public class EmailReceiver {

    String to;
    List<String> ccList;
    List<String> bccList;

    public List<String> getBccList() {
        return bccList;
    }

    public void setBccList(List<String> bccList) {
        this.bccList = bccList;
    }

    public String getTo() {
        return to;
    }

    public List<String> getCcList() {
        return ccList;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void setCcList(List<String> bccList) {
        this.ccList = bccList;
    }

}
