package com.project.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVWriter;
import com.project.application.Application;
import com.project.model.EmailReceiver;
import com.project.model.JobsModel;
import com.project.model.json.ExportSourcesModel;
import com.project.model.stats.StatsOuterModel;
import com.project.service.EmailSender;
import com.project.service.FtpExportService;
import com.project.service.JobService;
import com.project.service.StatsService;
import com.project.utility.ExportUtilities;
import com.project.utility.Utility;
import com.sun.xml.bind.marshaller.DataWriter;

@Controller
public class ExportController {

	@Autowired
	JobService jobService;

	@Autowired
	UrlShortnerController urlShortnerController;

	@Autowired
	FtpExportService ftpExportService;

	@Autowired
	List<ExportSourcesModel> exportSourceModelList;

	@Autowired
	HashMap<String, String> stateAndItsAbbrevation;

	@Value("${db.fetch.limit}")
	int limit;

	@Value("${thread.count}")
	int threadCount;
	
	
	@Value("${outer.thread.count}")
	int outerThreadCount;

	@Autowired
	StatsService statsService;

	@Autowired
	EmailSender emailSender;

	@Autowired
	@Qualifier("emailReceiverObject")
	EmailReceiver emailReceiver;

	@Autowired
	Utility utility;

	public void startProject() {

		System.out.println("size of stateAndItsAbbrevation=" + stateAndItsAbbrevation.size());

		if (!Application.isThisTestRun) {
			// try {
			// Runtime.getRuntime().exec("service mysql stop");
			// Thread.sleep(40 * 1000);
			// Runtime.getRuntime().exec("service mysql start");
			// Thread.sleep(4 * 60 * 1000);
			//
			// Utility.sendSms("Mysql service has been restarted from jobexport before starting process !!");
			//
			// } catch (Exception e) {
			// e.printStackTrace();
			// }

//			Utility.sendSms("Job export process has been started!!");
			EmailReceiver emailReceiverTest = new EmailReceiver();
			emailReceiverTest.setTo("pawan@signitysolutions.co.in");

			ArrayList<String> bccList = new ArrayList<String>();
			bccList.add("gurpreet.s@signitysolutions.in");
			bccList.add("nitika.chadha@signitysolutions.in");

			emailReceiverTest.setBccList(bccList);
			emailReceiverTest.setCcList(new ArrayList<String>());
//			emailSender.sendSmtpEmail("Job Export Process Has been started!!", "", emailReceiverTest, exportSourceModelList);

		}
		System.out.println("starting project");

		statsService.resetJobExportAbortMapToFlase(exportSourceModelList);

		for (ExportSourcesModel exportSourcesModel : exportSourceModelList) {

			if (Application.isThisTestRun) {
				if (exportSourcesModel.getName().equalsIgnoreCase(Application.forSingleExport)) {
					System.out.println("inside true block");
					startExportModelProcess(exportSourcesModel);

				}

			} else {

				startExportModelProcess(exportSourcesModel);

			}

			// export source for loop end
		}

		if (!Application.isThisTestRun) {

			// try {
			// Thread.sleep(2 * 60 * 1000);
			// Runtime.getRuntime().exec("service mysql stop");
			// Thread.sleep(30 * 1000);
			// Runtime.getRuntime().exec("service mysql start");
			//
			// Utility.sendSms("Mysql service has been restarted for jobexport in the end!!");
			//
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
		}
	}

	public void startExportModelProcess(ExportSourcesModel exportSourcesModel) {
		if (exportSourcesModel.isActive()) {
			try {

				ExportUtilities.dateWiseDailySent = new HashMap<String, Map<String, Integer>>();

				ExportUtilities.cpcWiseTotalSent = new HashMap<String, Integer>();

				ExportUtilities.totalJobsSent.set(0);

				int start = 0;

				String currentSource = exportSourcesModel.getName();

				DataWriter dataWriter = jobService.writeStartElement(exportSourcesModel);
				Marshaller marshaller = jobService.createMarshaller(currentSource);

				System.out.println("Start element written for : " + currentSource);
				exportSourcesModel.setQuery(jobService.updateDbQuery(exportSourcesModel).get(0));
				// new change
//				exportSourcesModel.setInitialCountquery(jobService.updateDbQuery(exportSourcesModel).get(1));
//				int totalCount = jobService.getJobsCountForExport(exportSourcesModel.getInitialCountquery());
//				System.out.println("totalCount" + totalCount);
//
//				int startIndex = 0;
//				int upto = 0;
//				int perThreadCount = totalCount / outerThreadCount;
//				for (int i = 0; i < outerThreadCount; i++) {
//					upto =perThreadCount;
//
//					System.out.println("thread count=" + start + "," + upto);
//					OuterThreadClass outerThreadClass = new
//							OuterThreadClass(dataWriter, exportSourcesModel,
//									marshaller, start, upto);
//					outerThreadClass.start();
//					start = start + perThreadCount;
//
//				}
//				System.exit(0);

				// below from this line we have compressed this code in method

				List<JobsModel> jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);

				while (jobsModeList != null && jobsModeList.size() > 0) {

					ExportUtilities.resultCounter.set(0);

					System.out.println("Jobs selected from db : " + jobsModeList.size());
					start = start + limit;

					List<List<JobsModel>> subLists = new ArrayList<List<JobsModel>>();
					subLists = Lists.partition(jobsModeList, jobsModeList.size() / threadCount);

					System.out.println("subLists size :" + subLists.size());
					System.out.println("threadCount : " + subLists.size());

					ExportUtilities.latch = new CountDownLatch(subLists.size());

					try {

						for (List<JobsModel> jobsList : subLists) {

							urlShortnerController.createJobsFeedFileWithThreads(jobsList, dataWriter, marshaller, currentSource, exportSourcesModel, stateAndItsAbbrevation);

						}

						ExportUtilities.latch.await();

					} catch (Exception e) {
						e.printStackTrace();
					}

					if (start >= exportSourcesModel.getJobExportLimit()) {
						System.out.println("reached max limit to export breaking now");
						break;
					}

					jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);
				} // While loop end

				System.out.println("writting end element");
				jobService.writeXmlEndElement(exportSourcesModel.getXmlStartEelementName(), dataWriter);

				StatsOuterModel statsOuterModel = statsService.updateStatsModel(exportSourcesModel.getName(), ExportUtilities.dateWiseDailySent);

				statsService.writeStatsModelInFile(exportSourcesModel.getName(), statsOuterModel);

				String fileSize = ftpExportService.uploadSFTPFile(exportSourcesModel);

				String emailBody = emailSender.createEmailBody(statsOuterModel, currentSource);
				String testSubject = "";
				if (Application.isThisTestRun) {
					testSubject = "TESTING EXPORT ";
				}
//
				emailSender.sendSmtpEmail(testSubject + " " + currentSource + "( " + fileSize + " ) Job Export Stats For " + utility.getTodayDate(), emailBody, emailReceiver,
						new ArrayList<ExportSourcesModel>());

				Utility.sendSms(testSubject + " " + currentSource + "( " + fileSize + " ) Job Export has been successfully done for " + utility.getTodayDate());

				try {
					if (Utility.cpc_more_than_one_point_two_map.size() != 0) {
						System.out.println("sending mail for high cpc");
						String cpc_more_that_export = "";
						cpc_more_that_export = new GsonBuilder().setPrettyPrinting().create().toJson(Utility.cpc_more_than_one_point_two_map);
						String body = "Follwing source from which we are getting cpc more than 1.2$.Please inform Jason for confirmation \n" + cpc_more_that_export;
						Utility.sendSms(body);
						emailSender.sendSmtpEmail("HIGH CPC ALERT !!!!", body, emailReceiver, new ArrayList<ExportSourcesModel>());

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public class OuterThreadClass extends Thread {
		ExportSourcesModel exportSourcesModel;
		DataWriter dataWriter;
		int start, limit;
		Marshaller marshaller;

		public OuterThreadClass(DataWriter dataWriter, ExportSourcesModel model, Marshaller marshaller, int start, int limit) {

			this.dataWriter = dataWriter;
			this.exportSourcesModel = model;
			this.marshaller = marshaller;
			this.start = start;
			this.limit = limit;

		}

		@Override
		public void run() {
			// TODO Auto-generated method stub

			List<JobsModel> jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);

			while (jobsModeList != null && jobsModeList.size() > 0) {

				ExportUtilities.resultCounter.set(0);

				System.out.println("Jobs selected from db : " + jobsModeList.size());
//				start = start + limit;

				List<List<JobsModel>> subLists = new ArrayList<List<JobsModel>>();
				subLists = Lists.partition(jobsModeList, jobsModeList.size() / threadCount);

				System.out.println("subLists size :" + subLists.size());
				System.out.println("threadCount : " + subLists.size());

				ExportUtilities.latch = new CountDownLatch(subLists.size());

				try {

					for (List<JobsModel> jobsList : subLists) {

						urlShortnerController.createJobsFeedFileWithThreads(jobsList, dataWriter, marshaller, exportSourcesModel.getName(), exportSourcesModel, stateAndItsAbbrevation);

					}

					ExportUtilities.latch.await();

				} catch (Exception e) {
					e.printStackTrace();
				}

//				if (start >= exportSourcesModel.getJobExportLimit()) {
//					System.out.println("reached max limit to export breaking now");
//					break;
//				}

//				jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);
			} // While loop end

		}

	}

	public void test() {

		System.out.println("hello");

	}
}
