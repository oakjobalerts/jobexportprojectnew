package com.project.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.project.model.EmailReceiver;
import com.project.model.json.ExportSourcesModel;
import com.project.model.stats.DateWiseStatsModel;
import com.project.model.stats.StatsOuterModel;
import com.project.service.EmailSender;
import com.project.utility.Utility;

@Service
public class EmailSenderImpl implements EmailSender {

	@Value("${mail.smtp.user}")
	String user;

	@Value("${mail.smtp.password}")
	String pass;

	@Value("${mail.smtp.host}")
	String mailSmtpHost;

	@Value("${mail.transport.protocol}")
	String mailTransportProtocol;

	@Value("${mail.smtp.port}")
	Integer mailSmtpPort;

	@Value("${mail.smtp.auth}")
	String mailSmtpAuth;

	@Value("${mail.smtp.socketFactory.port}")
	Integer mailSmtpSocketFactoryPort;

	@Value("${mail.smtp.socketFactory.class}")
	String mailSmtpSocketFactoryClass;

	@Value("${job.export.abort.api.url}")
	String jobExportAbortApiUrl;

	Integer cpcJobsShowLimit = 1000;

	@Autowired
	Utility utility;

	@Value("${export.sources.json.file.path}")
	String exportSourcesPath;

	@Override
	public String sendSmtpEmail(String subject, String body, EmailReceiver emailReceiver, List<ExportSourcesModel> list) {

		Properties props = System.getProperties();

		props.put("mail.smtp.host", mailSmtpHost);
		props.put("mail.transport.protocol", mailTransportProtocol);
		props.put("mail.smtp.port", mailSmtpPort);
		props.put("mail.smtp.auth", mailSmtpAuth);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.socketFactory.port", mailSmtpSocketFactoryPort);
		props.put("mail.smtp.socketFactory.class", mailSmtpSocketFactoryClass);

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		};

		Session session = Session.getInstance(props, auth);

		try {
			MimeMessage msg = new MimeMessage(session);

			for (String cc : emailReceiver.getCcList()) {
				msg.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
			}

			for (String bcc : emailReceiver.getBccList()) {
				msg.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
			}

			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setSubject(subject, "UTF-8");
			msg.setContent(body, "text/html");

			msg.setFrom(new InternetAddress(user, "FBG Reports"));
			msg.setReplyTo(InternetAddress.parse(user, false));

			msg.setSentDate(new Date());

			if (list.size() != 0) {

				MimeBodyPart messageBodyPart = new MimeBodyPart();
				DataSource ds = new ByteArrayDataSource(new GsonBuilder().setPrettyPrinting().create().toJson(list), "application/json");
				messageBodyPart.setDataHandler(new DataHandler(ds));
				messageBodyPart.setFileName("jobsExportSources.json");

				MimeBodyPart textBodyPart = new MimeBodyPart();
				textBodyPart.setContent(body, "text/html");

				MimeMultipart mimeMultipart = new MimeMultipart();
				mimeMultipart.addBodyPart(messageBodyPart);
				mimeMultipart.addBodyPart(textBodyPart);

				msg.setContent(mimeMultipart);
			}

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailReceiver.getTo(), false));
			Transport.send(msg);

			System.out.println("Email Sent Successfully!!");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Email Sent Successfully!!";
	}

	@Override
	public String createEmailBody(StatsOuterModel statsOuterModel, String sourceName) {

		String body = "";

		body = body + "<div>";

		// + "<table cellspacing='0' cellpadding='12' border='0' width='900'>"
		// + "<thead>"
		// + "<tr>"
		// + "<th style='border-bottom: 2px solid #C35D5D;'>Date</th>"
		// + "<th style='border-bottom: 2px solid #C35D5D;'>Total Sent</th>"
		// +
		// "<th style='border-bottom: 2px solid #C35D5D;'>Source Wise Sent</th>"
		// + "</tr>"
		// + "</thead>"
		//
		// + "<tbody style='text-align: center;'>";

		// List<String> dateListOfThisMonth =
		// utility.getDateListDescForThisMonth();
		List<String> dateListOfThisMonth = new ArrayList<String>();
		dateListOfThisMonth.add(utility.getTodayDate());

		Map<String, Integer> dateWiseTotalSent = statsOuterModel.getDateWiseTotalSent();
		// for (Entry<String, Integer> dateWiseTotalSent :
		// statsOuterModel.getDateWiseTotalSent().entrySet()) {

		for (String date : dateListOfThisMonth) {

			if (dateWiseTotalSent.get(date) == null) {
				continue;
			}

			body = body
					// + "<tr style='border-collapse:collapse'>"
					// + "<td>" + date + "</td>"
					// + "<td>" + dateWiseTotalSent.get(date) + "</td>"
					// + "<td style='border-left: 2px solid red'>"

					+ "<div style='margin: 30px;'>"
					+ "<p>Date : " + date + "</p>"
					+ "<p>Total Sent : " + dateWiseTotalSent.get(date) + "</p>"
					+ "</div>"
					+ ""
					+ ""
					+ "<table cellspacing='0' cellpadding='12' border='0' width='900'>"
					+ "<thead>"
					+ "<tr>"
					+ "<th style='border-bottom: 2px solid #C35D5D;'>Source Name</th>"
					+ "<th style='border-bottom: 2px solid #C35D5D;'>Total Sent</th>"
					+ "<th style='border-bottom: 2px solid #C35D5D;'>Top Cpc</th>"
					+ "<th style='border-bottom: 2px solid #C35D5D;'>Top Jobs Count</th>"
					+ "</tr>"
					+ "</thead>"
					+ "<tbody style='vertical-align: text-top;text-align: center;'>";

			if (statsOuterModel.getDateWiseDailySentMap() != null && statsOuterModel.getDateWiseDailySentMap().get(date) != null) {

				DateWiseStatsModel dateWiseStatsModel = statsOuterModel.getDateWiseDailySentMap().get(date); // dateWiseTotalSent.get(date)
				Map<String, Map<String, Integer>> sourceWiseDailySentMap = dateWiseStatsModel.getSourceWiseDailySentMap();

				for (Entry<String, Map<String, Integer>> sourceStats : sourceWiseDailySentMap.entrySet()) {

					body = body + "<tr style='border-collapse:collapse'>"
							+ "<td style='border-bottom: 2px solid #0e5029;'>" + sourceStats.getKey() + "</td>"
							+ "<td style='border-bottom: 2px solid #0e5029;'>" + sourceStats.getValue().get("total") + "</td>"
							+ "<td style='text-align: -webkit-center; border-bottom: 2px solid #0e5029;'>"
							+ "<table cellspacing='0' cellpadding='12' border='0'>"
							+ "<tbody style='text-align: center;'>";

					Map<String, Integer> topCpcMap = sortBykey(sourceStats.getValue());
					Map<String, Integer> topJobMap = sortByValue(sourceStats.getValue());

					int counter = 0;

					for (Entry<String, Integer> cpcWiseModelMap : topCpcMap.entrySet()) {

						if (cpcWiseModelMap.getKey().equals("total")) {
							continue;
						}
						if (counter > cpcJobsShowLimit) {
							break;
						}
						body = body + "<tr>"
								+ "<td>" + "<span style='color: blue; font-weight: bold;'>" + cpcWiseModelMap.getKey() + "</span>" + " : " + cpcWiseModelMap.getValue() + "</td>"
								+ " </tr>";
						counter = counter + 1;
					}

					body = body + "</tbody>"
							+ "</table>"
							+ "</td>";

					body = body + "<td style='text-align: -webkit-center; border-bottom: 2px solid #0e5029;'>"
							+ "<table cellspacing='0' cellpadding='12' border='0'>"
							+ "<tbody style='text-align: center;'>";

					counter = 0;
					for (Entry<String, Integer> cpcWiseModelMap : topJobMap.entrySet()) {
						if (cpcWiseModelMap.getKey().equals("total")) {
							continue;
						}
						if (counter > cpcJobsShowLimit) {
							break;
						}
						body = body + "<tr>"
								+ "<td>" + "<span style='color: blue; font-weight: bold;'>" + cpcWiseModelMap.getKey() + "</span>" + " : " + cpcWiseModelMap.getValue() + "</td>"
								+ " </tr>";
						counter = counter + 1;
					}

					body = body + "</tbody>"
							+ "</table>"
							+ "</td>";
				}

				body = body + "</tr>";
			}

			// body = body + "</tbody>"
			// + "</table>"
			// + "</td>"
			// + "</tr>";
		}

		body = body + "</tbody>" + "</table>" + "</div>";

		body = body + "<div style='text-align: right;max-width: 900px;margin: 20px;'>  "
				+ " <button style='width: 100px;height: 31px;background-color: red;border: none;'>"
				+ "<a href='" + jobExportAbortApiUrl + sourceName + "' style='font-weight: bold;  text-decoration: none; color: white;'>Abort</button>"
				+ " </div>";

		// System.out.println(body);
		return body;
	}

	private <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {

		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {

			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {

				return (o2.getValue()).compareTo(o1.getValue());
				// return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}

	private <K, V extends Comparable<? super V>> Map<K, V> sortBykey(Map<K, V> map) {

		List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {

			@Override
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {

				return (o2.getKey() + "").compareTo(o1.getKey() + "");
				// return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	// public List<String> getTopJobsCountFromMap(Map<String, Integer>
	// cpcWiseModelMap) {
	//
	// List<String> jList = new ArrayList<String>();
	//
	// for (Entry<String, Integer> map : cpcWiseModelMap.entrySet()) {
	// if (map.getKey().equals("total")) {
	// continue;
	// }
	// cpcList.add(map.getKey());
	// }
	//
	// Collections.sort(cpcList, Collections.reverseOrder());
	//
	// return cpcList;
	//
	// }

	// public List<String> getTopCpcFromMap(Map<String, Integer>
	// cpcWiseModelMap) {
	//
	// List<String> cpcList = new ArrayList<String>();
	//
	// for (Entry<String, Integer> map : cpcWiseModelMap.entrySet()) {
	// if (map.getKey().equals("total")) {
	// continue;
	// }
	// cpcList.add(map.getKey());
	// }
	//
	// Collections.sort(cpcList, Collections.reverseOrder());
	//
	// return cpcList;
	// }

}
