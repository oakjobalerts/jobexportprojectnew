package com.project.model;

public class Url {

    String source;

    String url;
    String cpc;
    String zipcode;
    String sourceName;
    String title;
    String gcpc;
    String sentDate;
    String city;
    String state;

    String job_country;

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getJob_country() {
        return job_country;
    }

    public void setJob_country(String job_country) {
        this.job_country = job_country;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getGcpc() {
        return gcpc;
    }

    public void setGcpc(String gcpc) {
        this.gcpc = gcpc;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCpc() {
        return cpc;
    }

    public void setCpc(String cpc) {
        this.cpc = cpc;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Url(String url, String cpc, String zipcode, String sourceName, String title) {
        super();
        this.url = url;
        this.cpc = cpc;
        this.zipcode = zipcode;
        this.sourceName = sourceName;
        this.title = title;
    }

    public Url() {
        super();
    }

}
