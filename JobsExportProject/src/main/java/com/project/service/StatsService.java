package com.project.service;

import java.util.List;
import java.util.Map;

import com.project.model.JobsModel;
import com.project.model.json.ExportSourcesModel;
import com.project.model.stats.StatsOuterModel;

public interface StatsService {

    String getCurrentStatsFullFilePath(String source);

    StatsOuterModel getStatsModel(String path);

    void writeStatsModelInFile(String exportSource, StatsOuterModel statsOuterModel);

    StatsOuterModel createEmptyOuterStatsMOdel();

    Map<String, Map<String, Integer>> getCpcCountMap(List<JobsModel> jobsListUpdateCpcWithShortnerUrl, Map<String, Map<String, Integer>> dateWiseDailySent);

    StatsOuterModel updateStatsModel(String exportSource, Map<String, Map<String, Integer>> dateWiseDailySent);

    void resetJobExportAbortMapToFlase(List<ExportSourcesModel> exportSourceModelList);

}
