package com.project.repository;

import java.util.HashMap;
import java.util.List;

import com.project.model.state;
import com.project.model.json.ExportSourceModelDB;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;

public interface MapConfigurationRepository {

	List<ExportSourcesModel> getExportSourcesList();

	List<SubIdModel> getSubIdModelList();

	List<ExportSourceModelDB> getExportSourcesListFromDataBase();

	HashMap<String, String> getStateAndItsAbbrevationMap();

}
