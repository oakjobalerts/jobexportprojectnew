package com.project.service;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.Marshaller;

import com.opencsv.CSVWriter;
import com.project.model.ExportGeneralModel;
import com.project.model.ExportJujuModel;
import com.project.model.ExportMacroModel;
import com.project.model.JobsModel;
import com.project.model.ZippiaModel;
import com.project.model.state;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.sun.xml.bind.marshaller.DataWriter;

public interface JobServiceMultipleThread {

	DataWriter writeStartElement(ExportSourcesModel exportSourcesModel);

	CSVWriter writeStartElement_for_csv(ExportSourcesModel exportSourcesModel);

	Marshaller createMarshaller(String source);

	List<JobsModel> getJobsFromDb(String query, int start, int limit);

	List<JobsModel> encryptUrlAndCutCpcHalfService(List<JobsModel> jobs, String urlEndParams, ExportSourcesModel exportSourceModel, HashMap<String, String> stateAndItsAbbrevation,
			String urlShortnerApi, String redirectionUrlStartUrl, List<SubIdModel> subIdModelList, String aIdParamName);

	List<ExportJujuModel> convertJobsModelIntoJujuExportModel(List<JobsModel> jobsModelList);

	List<ExportMacroModel> convertJobsModelIntoMacroExportModel(List<JobsModel> jobsModelList);

	List<ExportGeneralModel> convertJobsModelIntoGeneralExportModel(List<JobsModel> jobsModelList, String exporteSource);

	void writeFragmentedMarshlingForGeneralExportModel(ExportGeneralModel exportModel, DataWriter dataWriter, Marshaller marshaller);

	void writeFragmentedMarshlingForJujuExportModel(ExportJujuModel exportModel, DataWriter dataWriter, Marshaller marshaller);

	void writeFragmentedMarshlingForMacroExportModel(ExportMacroModel exportModel, DataWriter dataWriter, Marshaller marshaller);

	void writeXmlEndElement(String element, DataWriter dataWriter);

	List<ZippiaModel> convertJobsModelIntoZippiaExportModel(List<JobsModel> jobsListUpdateCpcWithShortnerUrl);

	void writeFragmentedMarshlingForZippiaExportModel(ZippiaModel exportModel, DataWriter dataWriter, Marshaller marshaller);

	List<JobsModel> viktreFeedSetup(List<JobsModel> jobs, ExportSourcesModel exportSourceModel, HashMap<String, String> stateAndItsAbbrevation, String urlShortnerApi, String redirectionUrlStartUrl);

	List<String> updateDbQuery(ExportSourcesModel exportSourcesModel);

	public ExportSourcesModel getUpdatedExportSourceModel(ExportSourcesModel model);

	int getJobsCountForExport(String query);

}
