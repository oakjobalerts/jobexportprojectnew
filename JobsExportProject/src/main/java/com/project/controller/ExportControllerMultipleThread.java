package com.project.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVWriter;
import com.project.application.Application;
import com.project.model.EmailReceiver;
import com.project.model.JobsModel;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.project.model.stats.StatsOuterModel;
import com.project.service.EmailSender;
import com.project.service.FtpExportService;
import com.project.service.JobService;
import com.project.service.StatsService;
import com.project.utility.ExportUtilities;
import com.project.utility.Utility;
import com.sun.xml.bind.marshaller.DataWriter;

@Controller
public class ExportControllerMultipleThread {

	@Autowired
	JobService jobService;

	// @Autowired
	// UrlShortnerControllerMultipleThread urlShortnerControllerMultipleThread;

	@Autowired
	FtpExportService ftpExportService;

	@Autowired
	List<SubIdModel> subIdModelList;

	@Autowired
	List<ExportSourcesModel> exportSourceModelList;

	@Autowired
	HashMap<String, String> stateAndItsAbbrevation;

	@Value("${db.fetch.limit}")
	int limit;

	@Value("${thread.count}")
	int threadCount;
	
	

	@Value("${thread.count.purejob}")
	int threadCount_forPurejob;

	@Value("${url.shortner.api}")
	String urlShortnerApi;

	@Value("${url.shortner.api.test.purejob}")
	String urlShortnerApi_forPurejob;

	@Value("${domain.redirection.url.start.param}")
	String redirectionUrlStartUrl;
	
	@Value("${domain.redirection.url.start.param.for.purejob}")
	String redirectionUrlStartUrl_forPurejob;

	@Value("${outer.thread.count}")
	int outerThreadCount;

	@Value("${a.id.param.name}")
	String aIdParamName;

	@Autowired
	StatsService statsService;

	@Autowired
	EmailSender emailSender;

	@Autowired
	@Qualifier("emailReceiverObject")
	EmailReceiver emailReceiver;

	@Autowired
	Utility utility;

	public void startProject() {

		System.out.println("size of stateAndItsAbbrevation=" + aIdParamName);
		if (!Application.isThisTestRun) {
			// try {
			// Runtime.getRuntime().exec("service mysql stop");
			// Thread.sleep(40 * 1000);
			// Runtime.getRuntime().exec("service mysql start");
			// Thread.sleep(4 * 60 * 1000);
			//
			// Utility.sendSms("Mysql service has been restarted from jobexport before starting process !!");
			//
			// } catch (Exception e) {
			// e.printStackTrace();
			// }

			Utility.sendSms("Job export process has been started!!");
			EmailReceiver emailReceiverTest = new EmailReceiver();
			emailReceiverTest.setTo("pawan@signitysolutions.co.in");

			ArrayList<String> bccList = new ArrayList<String>();
			bccList.add("gurpreet.s@signitysolutions.in");
			bccList.add("nitika.chadha@signitysolutions.in");

			emailReceiverTest.setBccList(bccList);
			emailReceiverTest.setCcList(new ArrayList<String>());
			emailSender.sendSmtpEmail("Job Export Process Has been started!!",
					"", emailReceiverTest, exportSourceModelList);

		}
		System.out.println("starting project");

		statsService.resetJobExportAbortMapToFlase(exportSourceModelList);

		for (ExportSourcesModel exportSourcesModel : exportSourceModelList) {

			if (Application.isThisTestRun) {
				//here we are doing this for purejobs
				urlShortnerApi = urlShortnerApi_forPurejob;
				redirectionUrlStartUrl=redirectionUrlStartUrl_forPurejob;
				threadCount=threadCount_forPurejob;
				System.out.println("urlShortnerApi"+urlShortnerApi+"redirectionUrlStartUrl="+redirectionUrlStartUrl);
				if (exportSourcesModel.getName().equalsIgnoreCase(Application.forSingleExport)) {
					System.out.println("inside true block");
					startExportModelProcess(exportSourcesModel);

				}

			} else {
				Utility.cpc_more_than_one_point_two_map.clear();
				startExportModelProcess(exportSourcesModel);

			}

			// export source for loop end
		}

		if (!Application.isThisTestRun) {

			// try {
			// Thread.sleep(2 * 60 * 1000);
			// Runtime.getRuntime().exec("service mysql stop");
			// Thread.sleep(30 * 1000);
			// Runtime.getRuntime().exec("service mysql start");
			//
			// Utility.sendSms("Mysql service has been restarted for jobexport in the end!!");
			//
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
		}
	}

	public void startExportModelProcess(ExportSourcesModel exportSourcesModel) {
		if (exportSourcesModel.isActive()) {
			try {

				ExportUtilities.dateWiseDailySent = new HashMap<String, Map<String, Integer>>();

				ExportUtilities.cpcWiseTotalSent = new HashMap<String, Integer>();

				ExportUtilities.totalJobsSent.set(0);

				int start = 0;

				String currentSource = exportSourcesModel.getName();

				DataWriter dataWriter = jobService.writeStartElement(exportSourcesModel);
				Marshaller marshaller = jobService.createMarshaller(currentSource);

				System.out.println("Start element written for : " + currentSource);
				exportSourcesModel.setQuery(jobService.updateDbQuery(exportSourcesModel).get(0));

				// below from this line we have compressed this code in method

				boolean isallJobProcessingDone = false;
				boolean isallThreadCompleted = true;

				List<JobsModel> jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);
				while (jobsModeList != null && jobsModeList.size() > 0) {
					if (isallThreadCompleted) {
						isallThreadCompleted = false;
						start = start + limit;
						List<List<JobsModel>> subLists = new ArrayList<List<JobsModel>>();

						int fromIndex = 0;
						int perListSize = jobsModeList.size() / threadCount;
						int toIndex = perListSize;
						for (int i = 0; i < threadCount; i++) {

							if (i == threadCount - 1) {
								toIndex = jobsModeList.size() - 1;
							}
							System.out.println("data size for this thread=" + fromIndex + "," + toIndex);

							List<JobsModel> jobsList = new ArrayList<JobsModel>(jobsModeList.subList(fromIndex, toIndex));
							UrlShortnerControllerMultipleThread urlShorterner = new UrlShortnerControllerMultipleThread(jobsList, dataWriter, marshaller, exportSourcesModel.getName(),
									exportSourcesModel,
									stateAndItsAbbrevation, urlShortnerApi, redirectionUrlStartUrl, subIdModelList, aIdParamName);
							urlShorterner.start();

							fromIndex = fromIndex + perListSize;
							toIndex = toIndex + perListSize;

						}

					}
					if (ExportUtilities.numberOfThreadCompleted.get() == threadCount) {
						System.out.println("number of thread completed=" + ExportUtilities.numberOfThreadCompleted.get());
						isallThreadCompleted = true;
						if (start >= exportSourcesModel.getJobExportLimit()) {
							System.out.println("reached max limit to export breaking now");
							break;
						}

						jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);
						if (jobsModeList.size() != 0) {
							isallJobProcessingDone = false;
						}
						else {

							isallJobProcessingDone = true;
						}
						ExportUtilities.numberOfThreadCompleted.set(0);
					}

				}
				// While loop end

				if (isallJobProcessingDone) {
					System.out.println("writting end element");
					jobService.writeXmlEndElement(exportSourcesModel.getXmlStartEelementName(), dataWriter);

					StatsOuterModel statsOuterModel = statsService.updateStatsModel(exportSourcesModel.getName(), ExportUtilities.dateWiseDailySent);

					statsService.writeStatsModelInFile(exportSourcesModel.getName(), statsOuterModel);

					String fileSize = ftpExportService.uploadSFTPFile(exportSourcesModel);

					String emailBody = emailSender.createEmailBody(statsOuterModel, currentSource);
					String testSubject = "";
					if (Application.isThisTestRun) {
						testSubject = "TESTING EXPORT ";
					}
					//
					emailSender.sendSmtpEmail(testSubject + " " + currentSource + "( " + fileSize + " ) Job Export Stats For " + utility.getTodayDate(), emailBody, emailReceiver,
							new ArrayList<ExportSourcesModel>());

					Utility.sendSms(testSubject + " " + currentSource + "( " + fileSize + " ) Job Export has been successfully done for " + utility.getTodayDate());

					try {
						if (Utility.cpc_more_than_one_point_two_map.size() != 0) {
							System.out.println("sending mail for high cpc");
							String cpc_more_that_export = "";
							cpc_more_that_export = new GsonBuilder().setPrettyPrinting().create().toJson(Utility.cpc_more_than_one_point_two_map);
							String body = "Follwing source from which we are getting cpc more than 1.2$.Please inform Jason for confirmation \n" + cpc_more_that_export;
							Utility.sendSms(body);
							emailSender.sendSmtpEmail("HIGH CPC ALERT !!!!", body, emailReceiver, new ArrayList<ExportSourcesModel>());

						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public class OuterThreadClass extends Thread {
		ExportSourcesModel exportSourcesModel;
		DataWriter dataWriter;
		int start, limit;
		Marshaller marshaller;

		public OuterThreadClass(DataWriter dataWriter, ExportSourcesModel model, Marshaller marshaller, int start, int limit) {

			this.dataWriter = dataWriter;
			this.exportSourcesModel = model;
			this.marshaller = marshaller;
			this.start = start;
			this.limit = limit;

		}

		@Override
		public void run() {
			// TODO Auto-generated method stub

			List<JobsModel> jobsModeList = jobService.getJobsFromDb(exportSourcesModel.getQuery(), start, limit);

			while (jobsModeList != null && jobsModeList.size() > 0) {

				// ExportUtilities.resultCounter.set(0);

				System.out.println("Jobs selected from db : " + jobsModeList.size());
				// start = start + limit;

				List<List<JobsModel>> subLists = new ArrayList<List<JobsModel>>();
				subLists = Lists.partition(jobsModeList, jobsModeList.size() / threadCount);

				System.out.println("subLists size :" + subLists.size());
				System.out.println("threadCount : " + subLists.size());

				ExportUtilities.latch = new CountDownLatch(subLists.size());

				try {

					for (List<JobsModel> jobsList : subLists) {

						// urlShortnerController.createJobsFeedFileWithThreads(jobsList,
						// dataWriter, marshaller, exportSourcesModel.getName(),
						// exportSourcesModel, stateAndItsAbbrevation);

					}

					ExportUtilities.latch.await();

				} catch (Exception e) {
					e.printStackTrace();
				}

				// if (start >= exportSourcesModel.getJobExportLimit()) {
				// System.out.println("reached max limit to export breaking now");
				// break;
				// }

				// jobsModeList =
				// jobService.getJobsFromDb(exportSourcesModel.getQuery(),
				// start, limit);
			} // While loop end

		}

	}

	public void test() {

		System.out.println("hello");

	}
}
