package com.project.configuration;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.gson.GsonBuilder;
import com.project.application.Application;
import com.project.model.EmailReceiver;
import com.project.model.state;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.project.repository.MapConfigurationRepository;

@Configuration
public class MapConfiguration {

	@Autowired
	MapConfigurationRepository mapConfigurationRepository;

	@Value("${email.receiver.file}")
	String emailReceiverJsonFile;

	@Value("${email.receiver.file,test.run}")
	String emailReceiverJsonFileTestRun;

	@Bean
	List<ExportSourcesModel> exportSourceModelList() {

		return mapConfigurationRepository.getExportSourcesList();
	}

	@Bean
	
	HashMap<String, String> stateAndItsAbbrevation(){
		return mapConfigurationRepository.getStateAndItsAbbrevationMap();
	}
	
	@Bean
	List<SubIdModel> subIdModelList() {

		return mapConfigurationRepository.getSubIdModelList();
	}

	@Bean(name = "emailReceiverObject")
	public EmailReceiver getEmailReceivers() {

		try {
			if (Application.isThisTestRun) {
				emailReceiverJsonFile = emailReceiverJsonFile;
			}

			Resource resource = new ClassPathResource(emailReceiverJsonFile);
			InputStream resourceInputStream = resource.getInputStream();

			BufferedReader reader = new BufferedReader(new InputStreamReader(resourceInputStream));

			EmailReceiver emailReceiver = new GsonBuilder().setPrettyPrinting().create().fromJson(reader, EmailReceiver.class);
			return emailReceiver;

		} catch (Exception e) {
			e.printStackTrace();
			return new EmailReceiver();
		}
	}

}
