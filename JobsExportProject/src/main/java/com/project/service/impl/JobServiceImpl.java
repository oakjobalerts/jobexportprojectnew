package com.project.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import antlr.StringUtils;

import com.google.gson.Gson;
import com.opencsv.CSVWriter;
import com.project.application.Application;
import com.project.model.EmailReceiver;
import com.project.model.ExportGeneralModel;
import com.project.model.ExportJujuModel;
import com.project.model.ExportMacroModel;
import com.project.model.JobsModel;
import com.project.model.Url;
import com.project.model.ZippiaModel;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.project.repository.JobsRepository;
import com.project.service.EmailSender;
import com.project.service.JobService;
import com.project.utility.ExportUtilities;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.xml.bind.marshaller.DataWriter;

@Service
public class JobServiceImpl implements JobService {

	@Autowired
	JobsRepository jobsRepository;

	@Value("${url.shortner.api}")
	String urlShortnerApi;

	@Value("${domain.redirection.url.start.param}")
	String redirectionUrlStartUrl;

	@Autowired
	List<SubIdModel> subIdModelList;

	@Value("${print.sys.out}")
	boolean isSysoutPrintingEnabled;

	@Value("${viktre.export.domain.redirection.url.start.param}")
	String viktreDomainStartUrl;

	@Value("${viktre.export.url.end.param}")
	String viktreEndParams;

	@Value("${db.initial.fetch.query}")
	String initialDbQuery;

	@Value("${db.initial.fetch.count.query}")
	String initialCountQuery;

	@Autowired
	@Qualifier("emailReceiverObject")
	EmailReceiver emailReceiver;

	@Autowired
	EmailSender emailSender;

	@Override
	public DataWriter writeStartElement(ExportSourcesModel exportSourcesModel) {

		return jobsRepository.writeStartElement(exportSourcesModel);
	}

	@Override
	public Marshaller createMarshaller(String source) {

		return jobsRepository.createMarshaller(source);
	}

	@Override
	public List<JobsModel> getJobsFromDb(String query, int start, int limit) {

		return jobsRepository.getJobsFromDbWithStartId(query, start, limit);
	}

	@Override
	public int getJobsCountForExport(String query) {

		return jobsRepository.getinitialCountFromDb(query);
	}

	@Override
	public List<String> updateDbQuery(ExportSourcesModel exportSourcesModel) {

		String query = initialDbQuery;
		String iniialCountQuery = initialCountQuery;
		String excludedTitles_String = "";
		List<String> updatedQueryList = new ArrayList<String>();

		if (exportSourcesModel.getExcludedTitles() != null && exportSourcesModel.getExcludedTitles().size() != 0) {

			excludedTitles_String = " and ( ";
			for (String excludedTitle : exportSourcesModel.getExcludedTitles()) {

				excludedTitles_String = excludedTitles_String
						+ " title not like '%" + excludedTitle + "%'"
						+ " and title not like '% " + excludedTitle + "%'"
						+ " and title not like '%" + excludedTitle + " %'"
						+ " and title not like '% " + excludedTitle + " %' and ";

			}
			excludedTitles_String = excludedTitles_String.replaceAll("and $", "");
			excludedTitles_String = excludedTitles_String + " )";
		}

		String includedTitles_String = "";
		if (exportSourcesModel.getIncludedTitles() != null && exportSourcesModel.getIncludedTitles().size() != 0) {

			includedTitles_String = " and ( ";
			for (String includedTitle : exportSourcesModel.getIncludedTitles()) {

				includedTitles_String = includedTitles_String
						+ " title like '%" + includedTitle + "%'"
						+ " or title  like '% " + includedTitle + "%'"
						+ " or title  like '%" + includedTitle + " %'"
						+ " or title  like '% " + includedTitle + " %' or ";

			}

			includedTitles_String = includedTitles_String.replaceAll("or $", "");
			includedTitles_String = includedTitles_String + " ) ";

		}

		String excludedSources_String = "";
		if (exportSourcesModel.getExcludedSources() != null && exportSourcesModel.getExcludedSources().size() != 0) {

			excludedSources_String = "";
			for (String excludedSources : exportSourcesModel.getExcludedSources()) {

				excludedSources_String = excludedSources_String + " and source_name !='" + excludedSources + "' ";

			}
		}

		String includedSources_String = "";
		if (exportSourcesModel.getIncludedSources() != null && exportSourcesModel.getIncludedSources().size() != 0) {

			includedSources_String = "and (";
			for (String includedSources : exportSourcesModel.getIncludedSources()) {

				includedSources_String = includedSources_String + "  source_name ='" + includedSources + "' or ";

			}
			includedSources_String = includedSources_String.replaceAll("or $", "");
			includedSources_String = includedSources_String + " ) ";

		}

		if (exportSourcesModel.getMinCpc() != null && !exportSourcesModel.getMinCpc().isEmpty()) {

			query = query + " and cpc >= " + exportSourcesModel.getMinCpc();
			iniialCountQuery = iniialCountQuery + " and cpc >= " + exportSourcesModel.getMinCpc();

		} else {
			System.out.println("minmum cpc is not set in the file");
			emailSender.sendSmtpEmail("Minmum cpc is not set for " + exportSourcesModel.getName() + " not provided", "", emailReceiver, new ArrayList<ExportSourcesModel>());
		}

		query = query + " " + excludedTitles_String + " " + includedTitles_String + " " + excludedSources_String + " " + includedSources_String;
		iniialCountQuery = iniialCountQuery + " " + excludedTitles_String + " " + includedTitles_String + " " + excludedSources_String + " " + includedSources_String;
		updatedQueryList.add(query);
		updatedQueryList.add(iniialCountQuery);

		return updatedQueryList;
	}

	@Override
	public List<JobsModel> encryptUrlAndCutCpcHalfService(List<JobsModel> jobs, String urlEndParams, ExportSourcesModel exportSourceModel, HashMap<String, String> stateAndItsAbbrevation) {

		// Create one connection per thread for shortener API
		Client client = new Client();
		WebResource webResource = client.resource(urlShortnerApi);

		String subIdDateString = ExportUtilities.uniqueIdDateFormatter.format(new Date());
		String todayDateString = ExportUtilities.todayDateFormatterForSentDate.format(new Date());

		List<JobsModel> jobsModelUrlUpdatedList = new ArrayList<JobsModel>();

		for (JobsModel jobsModel : jobs) {

			// here we are fetching common data of every job object which need
			// to same in all the 6 new title job
			String lastTitle = jobsModel.getTitle().trim();
			String lastCpc = jobsModel.getCpc().trim();
			String lastGcpc = jobsModel.getGcpc().trim();
			String lastUrl = jobsModel.getUrl().trim();
			String lastEmployer = jobsModel.getEmployer().trim();

			if (exportSourceModel.getMultiTitles() != null && exportSourceModel.getMultiTitles().size() != 0) {
				int count = 1;

				for (String newTitle : exportSourceModel.getMultiTitles()) {
					// here we are modifying the original tile of job with new
					// one
					newTitle = newTitle.replace("Original Job Title", lastTitle);
					newTitle = newTitle.replace("Company Name", lastEmployer);
					newTitle = newTitle.replace("Location - City", jobsModel.getCity());

					if (jobsModel.getZipcode() != null && !jobsModel.getZipcode().equalsIgnoreCase(""))
						newTitle = newTitle.replace("Zip Code", jobsModel.getZipcode());

					else
						newTitle = newTitle.replace("Zip Code", jobsModel.getCity());

					jobsModel.setTitle(newTitle);
					jobsModel.setTitle(replaceCharactersAndConvertToLowerCase(jobsModel.getTitle()));
					// Update unique job id
					long currentDateTime = System.currentTimeMillis();
					// creating Date from millisecond
					Date currentDate = new Date(currentDateTime);
					subIdDateString = ExportUtilities.uniqueIdDateFormatter_for_macro.format(currentDate);
					jobsModel.setJobId(subIdDateString);

					// here we are reset the value in jobmodel same as the first
					// one
					jobsModel.setUrl(lastUrl);
					jobsModel.setCpc(lastCpc);
					jobsModel.setGcpc(lastGcpc);

					// new code for multi title jobs we are update the company
					// field for 2 out of 6 jobs as discussed with amit sir

					if (count == 2) {
						jobsModel.setEmployer(lastEmployer + " " + jobsModel.getState());
					}
					else if (count == 4) {
						if (stateAndItsAbbrevation.get(jobsModel.getState()) != null)
							jobsModel.setEmployer(lastEmployer + " " + stateAndItsAbbrevation.get(jobsModel.getState()));

					} else {
						jobsModel.setEmployer(lastEmployer);
					}

					count++;

					// =================================================================================

					// Update subid of sources in url
					String subIdUpdatedUrl = jobsRepository.updateSubIdBySourceInUrl(jobsModel.getSourceName(), jobsModel.getUrl(), subIdModelList);
					jobsModel.setUrl(subIdUpdatedUrl);

					if (isSysoutPrintingEnabled) {
						System.out.println("url before encryption : " + subIdUpdatedUrl);
					}

					// Encrypt Job url
					Url url = new Url();
					url.setCpc(jobsModel.getCpc());
					url.setSourceName(jobsModel.getSourceName());
					url.setTitle(jobsModel.getTitle());
					url.setUrl(jobsModel.getUrl());
					url.setZipcode(jobsModel.getZipcode());
					url.setGcpc(jobsModel.getGcpc());
					url.setSentDate(todayDateString);
					url.setJob_country(jobsModel.getJob_country());
					url.setCity(jobsModel.getCity());
					url.setState(jobsModel.getState());
					String aIdParamName = "";
					String encodedurl = jobsRepository.encryptUrl(url, redirectionUrlStartUrl, urlEndParams, aIdParamName);

					if (isSysoutPrintingEnabled) {
						System.out.println("encodedurl : " + encodedurl);
					}

					if (encodedurl == null) {
						continue;
					}

					// Call URL Shortner api
					String shortUrl = jobsRepository.callUrlShortnerAPI(encodedurl, webResource);

					if (isSysoutPrintingEnabled) {
						System.out.println("shortUrl : " + shortUrl);
					}

					if (shortUrl == null) {
						continue;
					}
					jobsModel.setUrl(shortUrl);

					// Update cpc
					jobsModel.setCpc(jobsRepository.cutCpcStringToHalf(jobsModel.getCpc(), jobsModel));
					jobsModel.setGcpc(jobsRepository.cutCpcStringToHalf(jobsModel.getGcpc(), jobsModel));

					if (jobsModel.getCpc() == null) {
						continue;
					}

					JobsModel job = new Gson().fromJson(new Gson().toJson(jobsModel), JobsModel.class);
					// System.out.println("new=" + url.getTitle());
					// System.out.println("old" + jobsModel.getTitle());
					jobsModelUrlUpdatedList.add(job);

				}
			} else {

				// Update unique job id
				jobsModel.setJobId(subIdDateString + jobsModel.getId());

				// Update subid of sources in url
				String subIdUpdatedUrl = jobsRepository.updateSubIdBySourceInUrl(jobsModel.getSourceName(), jobsModel.getUrl(), subIdModelList);
				jobsModel.setUrl(subIdUpdatedUrl);

				jobsModel.setTitle(replaceCharactersAndConvertToLowerCase(jobsModel.getTitle()));

				if (isSysoutPrintingEnabled) {
					System.out.println("url before encryption : " + subIdUpdatedUrl);
				}

				// Encrypt Job url
				Url url = new Url();
				url.setCpc(jobsModel.getCpc());
				url.setSourceName(jobsModel.getSourceName());
				url.setTitle(jobsModel.getTitle());
				url.setUrl(jobsModel.getUrl());
				url.setZipcode(jobsModel.getZipcode());
				url.setGcpc(jobsModel.getGcpc());
				url.setSentDate(todayDateString);
				url.setJob_country(jobsModel.getJob_country());
				url.setCity(jobsModel.getCity());
				url.setState(jobsModel.getState());
				String aIdParamName = "";
				String encodedurl = jobsRepository.encryptUrl(url, redirectionUrlStartUrl, urlEndParams, aIdParamName);

				if (isSysoutPrintingEnabled) {
					System.out.println("encodedurl : " + encodedurl);
				}

				if (encodedurl == null) {
					continue;
				}

				// Call URL Shortner api
				String shortUrl = jobsRepository.callUrlShortnerAPI(encodedurl, webResource);

				if (isSysoutPrintingEnabled) {
					System.out.println("shortUrl : " + shortUrl);
				}

				if (shortUrl == null) {
					continue;
				}
				jobsModel.setUrl(shortUrl);

				// Update cpc
				jobsModel.setCpc(jobsRepository.cutCpcStringToHalf(jobsModel.getCpc(), jobsModel));
				jobsModel.setGcpc(jobsRepository.cutCpcStringToHalf(jobsModel.getGcpc(), jobsModel));

				if (jobsModel.getCpc() == null) {
					continue;
				}

				JobsModel job = new Gson().fromJson(new Gson().toJson(jobsModel), JobsModel.class);

				jobsModelUrlUpdatedList.add(job);

			}

		}

		return jobsModelUrlUpdatedList;
	}

	@Override
	public List<JobsModel> viktreFeedSetup(List<JobsModel> jobs, ExportSourcesModel exportSourceModel, HashMap<String, String> stateAndItsAbbrevation) {

		String subIdDateString = "";

		List<JobsModel> jobsModelUrlUpdatedList = new ArrayList<JobsModel>();

		for (JobsModel jobsModel : jobs) {

			// Update unique job id
			// jobsModel.setJobId(subIdDateString + jobsModel.getId());
			//
			// jobsModel.setTitle(replaceCharactersAndConvertToLowerCase(jobsModel.getTitle()));

			// JobsModel job = new Gson().fromJson(new Gson().toJson(jobsModel),
			// JobsModel.class);
			//
			// jobsModelUrlUpdatedList.add(job);

			// here we are fetching common data of every job object which need
			// to same in all the 6 new title job
			String lastTitle = jobsModel.getTitle().trim();
			String lastEmployer = jobsModel.getEmployer().trim();

			if (exportSourceModel.getMultiTitles() != null && exportSourceModel.getMultiTitles().size() != 0) {
				int count = 1;

				for (String newTitle : exportSourceModel.getMultiTitles()) {
					// here we are modifying the original tile of job with new
					// one
					newTitle = newTitle.replace("Original Job Title", lastTitle);
					newTitle = newTitle.replace("Company Name", lastEmployer);
					newTitle = newTitle.replace("Location - City", jobsModel.getCity());

					if (jobsModel.getZipcode() != null && !jobsModel.getZipcode().equalsIgnoreCase(""))
						newTitle = newTitle.replace("Zip Code", jobsModel.getZipcode());

					else
						newTitle = newTitle.replace("Zip Code", jobsModel.getCity());

					jobsModel.setTitle(newTitle);
					jobsModel.setTitle(replaceCharactersAndConvertToLowerCase(jobsModel.getTitle()));
					// Update unique job id
					long currentDateTime = System.currentTimeMillis();
					// creating Date from millisecond
					Date currentDate = new Date(currentDateTime);
					subIdDateString = ExportUtilities.uniqueIdDateFormatter_for_macro.format(currentDate);
					jobsModel.setJobId(subIdDateString);

					// here we are reset the value in jobmodel same as the first
					// one

					if (count == 2) {
						jobsModel.setEmployer(lastEmployer + " " + jobsModel.getState());
					}
					else if (count == 4) {
						if (stateAndItsAbbrevation.get(jobsModel.getState()) != null)
							jobsModel.setEmployer(lastEmployer + " " + stateAndItsAbbrevation.get(jobsModel.getState()));

					} else {
						jobsModel.setEmployer(lastEmployer);
					}

					count++;

					// =================================================================================

					JobsModel job = new Gson().fromJson(new Gson().toJson(jobsModel), JobsModel.class);
					// System.out.println("new=" + url.getTitle());
					// System.out.println("old" + jobsModel.getTitle());
					jobsModelUrlUpdatedList.add(job);

				}
			}

		}

		return jobsModelUrlUpdatedList;
	}

	@Override
	public List<ExportJujuModel> convertJobsModelIntoJujuExportModel(List<JobsModel> jobsModelList) {

		List<ExportJujuModel> exportModelList = new ArrayList<ExportJujuModel>();

		for (JobsModel jobsModel : jobsModelList) {

			ExportJujuModel exportModel = new ExportJujuModel();

			exportModel.setId(jobsModel.getJobId());

			exportModel.setEmployer(jobsModel.getEmployer());
			exportModel.setTitle(jobsModel.getTitle());

			exportModel.setDescription(jobsModel.getTitle() + " - " + jobsModel.getEmployer() + "\n" + jobsModel.getDescription());

			exportModel.setPostingdate(jobsModel.getPostingDate());
			exportModel.setJoburl(jobsModel.getUrl());

			exportModel.setLocation("<nation></nation>" + "<city><![CDATA[ " + jobsModel.getCity() + " ]]></city>" + "<county></county>" + "<state><![CDATA[ " + jobsModel.getState() + " ]]></state>"
					+ "<zip><![CDATA[ " + jobsModel.getZipcode() + " ]]></zip>");

			exportModel.setSalary("<period></period>" + "<min></min>" + "<max></max>");

			exportModel.setType(jobsModel.getJobType());
			exportModel.setExperience("<min></min><max></max>");
			exportModel.setEducation("");
			// exportModel.setJobsource(jobsModel.getSourceName());
			exportModel.setJobsourceurl("");

			exportModel.setCpc(jobsModel.getCpc());

			exportModelList.add(exportModel);

		}

		// Type type = new TypeToken<List<ExportJujuModel>>() {
		// private static final long serialVersionUID = 1L;
		// }.getType();
		//
		// List<ExportJujuModel> exportModelList = new Gson().fromJson(new
		// Gson().toJson(jobsModelList), type);
		//
		// System.out.println(new Gson().toJson(exportModelList));
		return exportModelList;
	}

	@Override
	public List<ExportGeneralModel> convertJobsModelIntoGeneralExportModel(List<JobsModel> jobsModelList, String exportSource) {

		List<ExportGeneralModel> exportModelList = new ArrayList<ExportGeneralModel>();

		for (JobsModel jobsModel : jobsModelList) {

			ExportGeneralModel exportModel = new ExportGeneralModel();

			exportModel.setJobId(jobsModel.getJobId());
			exportModel.setCategory("");
			exportModel.setReferencenumber("");
			exportModel.setUrl(jobsModel.getUrl());
			exportModel.setCity(jobsModel.getCity());
			exportModel.setState(jobsModel.getState());
			exportModel.setCountry(jobsModel.getJob_country());
			exportModel.setDescription(jobsModel.getTitle() + " - " + jobsModel.getEmployer() + "\n" + jobsModel.getDescription());

			exportModel.setCompany(jobsModel.getEmployer());
			exportModel.setDate(jobsModel.getPostingDate());
			exportModel.setSalary("");
			exportModel.setEducation("");
			exportModel.setJobtype(jobsModel.getJobType());
			exportModel.setCategory(jobsModel.getCategory());
			exportModel.setExperience("");

			// here we are only sending original source in export for viktre
			// only as
			// discussed with jason
			if (exportSource.equalsIgnoreCase("viktre")) {
				exportModel.setSource(jobsModel.getOriginal_source());
			}

			// here we are not providing following parameter in xml for macro
			// job export as discussed with jason on 27-10-2017

			String location = jobsModel.getCity() != "" ? jobsModel.getCity() : "";
			location = jobsModel.getState() != "" ? location + ", " + jobsModel.getState() : location + "";
			exportModel.setLocation(location);
			exportModel.setTitle(jobsModel.getTitle());

			exportModel.setCpc(jobsModel.getCpc());

			exportModelList.add(exportModel);

		}

		// Type type = new TypeToken<List<ExportGeneralModel>>() {
		// private static final long serialVersionUID = 2L;
		// }.getType();
		//
		// List<ExportGeneralModel> exportModelList = new Gson().fromJson(new
		// Gson().toJson(jobsModelList), type);
		//
		// System.out.println(new Gson().toJson(exportModelList));
		return exportModelList;
	}

	@Override
	public synchronized void writeFragmentedMarshlingForGeneralExportModel(ExportGeneralModel exportModel, DataWriter dataWriter, Marshaller marshaller) {
		try {
			marshaller.marshal(exportModel, dataWriter);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public CSVWriter writeStartElement_for_csv(ExportSourcesModel exportSourcesModel) {

		return jobsRepository.writeCsvFileInLocal(exportSourcesModel);
	}

	public synchronized void writeExportModelIn_Csv(ExportGeneralModel exportModel, CSVWriter csvWriter) {
		try {
			// csvWriter.wri
			// marshaller.marshal(exportModel, dataWriter);
			// String[] firstRow = { "jobId",
			// "title",
			// "category",
			// "city",
			// "state",
			// "zipcode",
			// "source",
			// "cpc",
			// "jobType",
			// "description",
			// "company",
			// "url",
			// "postingdate", };
			String dataArray[] = { exportModel.getJobId(), exportModel.getTitle(), exportModel.getCategory(), exportModel.getCity()
					, exportModel.getState(), exportModel.getLocation(), exportModel.getSource(), exportModel.getCpc(), exportModel.getJobtype()
					, exportModel.getDescription(), exportModel.getCompany(), exportModel.getUrl(), exportModel.getDate(),

			};
			csvWriter.writeNext(dataArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void writeFragmentedMarshlingForJujuExportModel(ExportJujuModel exportModel, DataWriter dataWriter, Marshaller marshaller) {
		try {
			marshaller.marshal(exportModel, dataWriter);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public synchronized void writeXmlEndElement(String element, DataWriter dataWriter) {

		try {

			dataWriter.characters("</" + element + ">");
			dataWriter.endDocument();
			dataWriter.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	String replaceCharactersAndConvertToLowerCase(String value) {

		value = value.replace(",", "").replace("*", "").replace("/", "").replace("?", "").replace("&", "").replace("<", "").replace(">", "").replace("\"", "")
				.replace("(", "").replace(")", "").replace("$", "").replace(":", "").replace("'", "").replace("!", "").replace("%", "").replace("-", "");

		return convertFirstChartoUpper(value.toLowerCase());

	}

	String convertFirstChartoUpper(String value) {
		// using org.apache.commons.lang3.text.WordUtils
		return WordUtils.capitalize(value);

	}

	// String replaceCharactersAndConvertToLowerCase_macro(String value) {
	//
	// return value.replace(",", "").replace("*", "").replace("/",
	// "").replace("?", "").replace("&", "").replace("<", "").replace(">",
	// "").replace("\"", "")
	// .replace("(", "").replace(")", "").replace("$", "").replace(":",
	// "").replace("'", "").replace("!", "").replace("%", "").replace("-", "");
	//
	// }

	@Override
	public List<ZippiaModel> convertJobsModelIntoZippiaExportModel(List<JobsModel> jobsModelList) {

		List<ZippiaModel> exportModelList = new ArrayList<ZippiaModel>();

		for (JobsModel jobsModel : jobsModelList) {

			ZippiaModel exportModel = new ZippiaModel();

			exportModel.setId(jobsModel.getJobId());
			exportModel.setTitle(jobsModel.getTitle());
			exportModel.setCompany(jobsModel.getEmployer());
			exportModel.setDescription(jobsModel.getTitle() + " - " + jobsModel.getEmployer() + "\n" + jobsModel.getDescription());
			exportModel.setCity(jobsModel.getCity());
			exportModel.setState(jobsModel.getState());
			exportModel.setCountry(jobsModel.getJob_country());
			exportModel.setZipcode(jobsModel.getZipcode());
			exportModel.setPostingdate(jobsModel.getPostingDate());
			exportModel.setIndustry(jobsModel.getCategory());
			exportModel.setUrl(jobsModel.getUrl());
			exportModel.setCpc(jobsModel.getCpc());
			exportModel.setCpa(jobsModel.getCpc());
			exportModel.setPrice("");

			exportModelList.add(exportModel);
		}

		return exportModelList;
	}

	public String updateViktreUrl(String jobReference) {

		String url = viktreDomainStartUrl + jobReference + viktreEndParams;

		return url;
	}

	@Override
	public synchronized void writeFragmentedMarshlingForZippiaExportModel(ZippiaModel exportModel, DataWriter dataWriter, Marshaller marshaller) {

		try {
			marshaller.marshal(exportModel, dataWriter);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<ExportMacroModel> convertJobsModelIntoMacroExportModel(List<JobsModel> jobsModelList) {

		List<ExportMacroModel> exportModelList = new ArrayList<ExportMacroModel>();

		for (JobsModel jobsModel : jobsModelList) {

			ExportMacroModel exportModel = new ExportMacroModel();

			exportModel.setJobId(jobsModel.getJobId());
			exportModel.setUrl(jobsModel.getUrl());
			exportModel.setCity(jobsModel.getCity());
			exportModel.setState(jobsModel.getState());
			exportModel.setCountry(jobsModel.getJob_country());
			exportModel.setDescription(jobsModel.getTitle() + " - " + jobsModel.getEmployer() + "\n" + jobsModel.getDescription());
			exportModel.setCompany(jobsModel.getEmployer());
			exportModel.setDate(jobsModel.getPostingDate());

			// here we are only sending original source in export for viktre
			// only as
			// discussed with jason

			// here we are not providing following parameter in xml for macro
			// job export as discussed with jason on 27-10-2017

			String location = jobsModel.getCity() != "" ? jobsModel.getCity() : "";
			location = jobsModel.getState() != "" ? location + ", " + jobsModel.getState() : location + "";

			exportModel.setLocation(jobsModel.getZipcode());
			exportModel.setTitle(jobsModel.getTitle());

			exportModel.setCpc(jobsModel.getCpc());

			exportModelList.add(exportModel);

		}

		return exportModelList;
	}

	@Override
	public synchronized void writeFragmentedMarshlingForMacroExportModel(ExportMacroModel exportModel, DataWriter dataWriter, Marshaller marshaller) {
		// TODO Auto-generated method stub

		try {
			marshaller.marshal(exportModel, dataWriter);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public ExportSourcesModel getUpdatedExportSourceModel(ExportSourcesModel model) {
		// TODO Auto-generated method stub

		return null;
	}

}
