package com.project.model.stats;

import java.util.Map;

public class DateWiseStatsModel {

    Map<String, Map<String, Integer>> sourceWiseDailySentMap;

    public Map<String, Map<String, Integer>> getSourceWiseDailySentMap() {
        return sourceWiseDailySentMap;
    }

    public void setSourceWiseDailySentMap(Map<String, Map<String, Integer>> sourceWiseDailySentMap) {
        this.sourceWiseDailySentMap = sourceWiseDailySentMap;
    }

}
