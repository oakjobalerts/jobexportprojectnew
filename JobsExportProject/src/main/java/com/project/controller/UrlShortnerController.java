package com.project.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.opencsv.CSVWriter;
import com.project.model.ExportGeneralModel;
import com.project.model.ExportJujuModel;
import com.project.model.ExportMacroModel;
import com.project.model.JobsModel;
import com.project.model.ZippiaModel;
import com.project.model.json.ExportSourcesModel;
import com.project.service.JobService;
import com.project.service.StatsService;
import com.project.utility.ExportUtilities;
import com.sun.xml.bind.marshaller.DataWriter;

@Component
public class UrlShortnerController {

	@Autowired
	JobService jobService;

	@Autowired
	StatsService statsService;

	public boolean createJobsFeedFileWithThreads(List<JobsModel> jobsList, final DataWriter dataWriter, final Marshaller marshaller, String source, final ExportSourcesModel exportSourceModel,
			final HashMap<String, String> stateAndItsAbbrevation) {

		final List<JobsModel> jobsListfinal = new ArrayList<JobsModel>(jobsList);

		final String finalSource = source;
		final String threadAId = exportSourceModel.getA_id();

		new Thread(new Runnable() {
			@Override
			public void run() {

				System.out.println("run called for thread : " + ExportUtilities.resultCounter.incrementAndGet());

				try {

					List<JobsModel> jobsListUpdateCpcWithShortnerUrl = null;

					if (finalSource.toLowerCase().equals("juju")) {

						jobsListUpdateCpcWithShortnerUrl = jobService.encryptUrlAndCutCpcHalfService(jobsListfinal, threadAId, exportSourceModel, stateAndItsAbbrevation);
						List<ExportJujuModel> exportModels = jobService.convertJobsModelIntoJujuExportModel(jobsListUpdateCpcWithShortnerUrl);
						Collections.shuffle(exportModels);
						for (ExportJujuModel exportModel : exportModels) {

							jobService.writeFragmentedMarshlingForJujuExportModel(exportModel, dataWriter, marshaller);
						}
					} else if (finalSource.toLowerCase().equals("zippia")) {

						jobsListUpdateCpcWithShortnerUrl = jobService.encryptUrlAndCutCpcHalfService(jobsListfinal, threadAId, exportSourceModel, stateAndItsAbbrevation);
						List<ZippiaModel> exportModels = jobService.convertJobsModelIntoZippiaExportModel(jobsListUpdateCpcWithShortnerUrl);
						Collections.shuffle(exportModels);
						for (ZippiaModel exportModel : exportModels) {

							jobService.writeFragmentedMarshlingForZippiaExportModel(exportModel, dataWriter, marshaller);
						}
						// we are not cutting half cpc for viktre
					} else if (finalSource.toLowerCase().equals("viktre")) {

						jobsListUpdateCpcWithShortnerUrl = jobService.viktreFeedSetup(jobsListfinal, exportSourceModel, stateAndItsAbbrevation);
						List<ExportGeneralModel> exportModels = jobService.convertJobsModelIntoGeneralExportModel(jobsListUpdateCpcWithShortnerUrl, "viktre");
						Collections.shuffle(exportModels);
						for (ExportGeneralModel exportModel : exportModels) {

							jobService.writeFragmentedMarshlingForGeneralExportModel(exportModel, dataWriter, marshaller);
						}
					}
					else if (finalSource.toLowerCase().equals("macro") || finalSource.toLowerCase().equals("jobtome")
							||finalSource.toLowerCase().equals("nexxt")
							) {

						jobsListUpdateCpcWithShortnerUrl = jobService.encryptUrlAndCutCpcHalfService(jobsListfinal, threadAId, exportSourceModel, stateAndItsAbbrevation);
						List<ExportMacroModel> exportModels = jobService.convertJobsModelIntoMacroExportModel(jobsListUpdateCpcWithShortnerUrl);
						Collections.shuffle(exportModels);
						for (ExportMacroModel exportModel : exportModels) {

							jobService.writeFragmentedMarshlingForMacroExportModel(exportModel, dataWriter, marshaller);
						}

					}

					else {

						jobsListUpdateCpcWithShortnerUrl = jobService.encryptUrlAndCutCpcHalfService(jobsListfinal, threadAId, exportSourceModel, stateAndItsAbbrevation);
						List<ExportGeneralModel> exportModels = jobService.convertJobsModelIntoGeneralExportModel(jobsListUpdateCpcWithShortnerUrl, "general");
						Collections.shuffle(exportModels);
						for (ExportGeneralModel exportModel : exportModels) {

							jobService.writeFragmentedMarshlingForGeneralExportModel(exportModel, dataWriter, marshaller);
						}
					}

					Map<String, Map<String, Integer>> dateWiseDailySent = ExportUtilities.dateWiseDailySent;
					dateWiseDailySent = statsService.getCpcCountMap(jobsListUpdateCpcWithShortnerUrl, dateWiseDailySent);

				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					ExportUtilities.latch.countDown();
					System.out.println(ExportUtilities.latch.getCount());
				}
			}
		}).start();

		return false;
	}
}
