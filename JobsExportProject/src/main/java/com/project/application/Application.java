package com.project.application;

import java.util.ArrayList;

import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import ch.qos.logback.classic.pattern.Util;

import com.project.controller.ExportController;
import com.project.controller.ExportControllerMultipleThread;
import com.project.model.EmailReceiver;
import com.project.service.EmailSender;
import com.project.utility.Utility;

@SpringBootApplication
@ComponentScan(basePackages = "com.project")
@PropertySource({ "classpath:/export.properties" })
@PropertySource("classpath:/smtp.properties")
public class Application implements CommandLineRunner {

	public static boolean isThisTestRun = false;
	public static String forSingleExport = "";
	public static boolean isAttachment = false;

	// @Autowired
	// ExportController homeController;

	@Autowired
	ExportControllerMultipleThread homeController;

	@Autowired
	EmailSender emailSender;

	@Autowired
	@Qualifier("emailReceiverObject")
	EmailReceiver emailReceiver;

	public static void main(String[] args) {

		if (args != null && args.length > 0) {
			// if (args[0].equalsIgnoreCase("runtest")) {
			Application.isThisTestRun = true;
			Application.forSingleExport = args[0];
			System.out.println("runtest is" + isThisTestRun);

			// }
		}

		SpringApplication.run(Application.class, args);

		// SpringApplication app = new SpringApplication(Application.class);
		// app.setBannerMode(Banner.Mode.OFF);
		// // app.setWebEnvironment(false);
		// app.run(args);

	}

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory emf) {
		HibernateJpaSessionFactoryBean factory = new HibernateJpaSessionFactoryBean();
		factory.setEntityManagerFactory(emf);
		return factory;
	}

	// @Bean
	// public CommandLineRunner commandLineRunner(ApplicationContext context) {
	// System.out.println("in commandLineRunner");
	// return args -> {
	// context.getBeanDefinitionNames();
	// };
	// }

	@Override
	public void run(String... args) throws Exception {
		homeController.startProject();

	}

}
