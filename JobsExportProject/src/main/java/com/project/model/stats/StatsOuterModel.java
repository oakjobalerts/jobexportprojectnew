package com.project.model.stats;

import java.util.Map;

public class StatsOuterModel {

    String date;
    String exportSource;
    Integer totalSent;

    Map<String, Integer> dateWiseTotalSent;
    Map<String, Integer> cpcWiseTotalSent;

    Map<String, DateWiseStatsModel> dateWiseDailySentMap;

    public String getExportSource() {
        return exportSource;
    }

    public void setExportSource(String exportSource) {
        this.exportSource = exportSource;
    }

    public String getDate() {
        return date;
    }

    public Integer getTotalSent() {
        return totalSent;
    }

    public Map<String, Integer> getDateWiseTotalSent() {
        return dateWiseTotalSent;
    }

    public Map<String, Integer> getCpcWiseTotalSent() {
        return cpcWiseTotalSent;
    }

    public Map<String, DateWiseStatsModel> getDateWiseDailySentMap() {
        return dateWiseDailySentMap;
    }

    public void setDateWiseDailySentMap(Map<String, DateWiseStatsModel> dateWiseDailySentMap) {
        this.dateWiseDailySentMap = dateWiseDailySentMap;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTotalSent(Integer totalSent) {
        this.totalSent = totalSent;
    }

    public void setDateWiseTotalSent(Map<String, Integer> dateWiseTotalSent) {
        this.dateWiseTotalSent = dateWiseTotalSent;
    }

    public void setCpcWiseTotalSent(Map<String, Integer> cpcWiseTotalSent) {
        this.cpcWiseTotalSent = cpcWiseTotalSent;
    }

}
