package com.project.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "job")
@XmlAccessorType(XmlAccessType.FIELD)
public class ZippiaModel {

    // TODO IMPORTANT : change setters methods to include cdata format

    String id;
    String title;
    String company;
    String description;
    String city;
    String state;
    String country;
    String zipcode;
    String postingdate;
    String industry;
    String url;
    String cpc;
    String cpa;
    String price;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCompany() {
        return company;
    }

    public String getDescription() {
        return description;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getPostingdate() {
        return postingdate;
    }

    public String getIndustry() {
        return industry;
    }

    public String getUrl() {
        return url;
    }

    public String getCpc() {
        return cpc;
    }

    public String getCpa() {
        return cpa;
    }

    public String getPrice() {
        return price;
    }

    public void setId(String id) {
        this.id = "<![CDATA[" + id + " ]]>";
    }

    public void setTitle(String title) {
        this.title = "<![CDATA[" + title + " ]]>";
    }

    public void setCompany(String company) {
        this.company = "<![CDATA[" + company + " ]]>";
    }

    public void setDescription(String description) {
        this.description = "<![CDATA[" + description + " ]]>";
    }

    public void setCity(String city) {
        this.city = "<![CDATA[" + city + " ]]>";
    }

    public void setState(String state) {
        this.state = "<![CDATA[" + state + " ]]>";
    }

    public void setCountry(String country) {
        this.country = "<![CDATA[" + country + " ]]>";
    }

    public void setZipcode(String zipcode) {
        this.zipcode = "<![CDATA[" + zipcode + " ]]>";
    }

    public void setPostingdate(String postingdate) {
        this.postingdate = "<![CDATA[" + postingdate + " ]]>";
    }

    public void setIndustry(String industry) {
        this.industry = "<![CDATA[" + industry + " ]]>";
    }

    public void setUrl(String url) {
        this.url = "<![CDATA[" + url + " ]]>";
    }

    public void setCpc(String cpc) {
        this.cpc = "<![CDATA[" + cpc + " ]]>";
    }

    public void setCpa(String cpa) {
        this.cpa = "<![CDATA[" + cpa + " ]]>";
    }

    public void setPrice(String price) {
        this.price = "<![CDATA[" + price + " ]]>";
    }

    // <job>
    // <id>238009151</id>
    // <title>Travel RN/Registered Nurse</title>
    // <company>PPR Travel Nursing</company>
    // <description>Travel RStaff Weekly</description>
    // <city>San Jose</city>
    // <state>CA</state>
    // <country>USA</country>
    // <zipcode>92154</zipcode>
    // <postingdate>2017-08-08 20:19:10</postingdate>
    // <industry>Nursing</industry>
    // <url>http://www.samplejob.com/?id=e4607d465f9c</url>
    // <cpc>$0.15</cpc>
    // <cpa></cpa>
    // <price></price>
    // </job>

}
