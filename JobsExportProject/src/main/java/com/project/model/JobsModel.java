package com.project.model;

import java.math.BigInteger;

import com.google.gson.annotations.SerializedName;

public class JobsModel {

	@SerializedName(value = "skip")
	BigInteger id;

	@SerializedName(value = "id")
	String jobId;

	@SerializedName(value = "title")
	String title;
	@SerializedName(value = "category")
	String category;
	@SerializedName(value = "city")
	String city;
	@SerializedName(value = "state")
	String state;

	@SerializedName(value = "zipcode")
	String zipcode;

	@SerializedName(value = "source")
	String sourceName;
	@SerializedName(value = "cpc")
	String cpc;
	@SerializedName(value = "jobType")
	String jobType;
	@SerializedName(value = "description")
	String description;
	@SerializedName(value = "company")
	String employer;
	@SerializedName(value = "url")
	String url;
	@SerializedName(value = "postingdate")
	String postingDate;
	@SerializedName(value = "gcpc")
	String gcpc;

	String jobReference;

	String job_country;
	@SerializedName(value = "original_source")
	String original_source;

	public String getOriginal_source() {
		return original_source;
	}

	public void setOriginal_source(String original_source) {
		this.original_source = original_source;
	}

	public String getJobReference() {
		return jobReference;
	}

	public void setJobReference(String jobReference) {
		this.jobReference = jobReference;
	}

	public String getJob_country() {
		return job_country;
	}

	public void setJob_country(String job_country) {
		this.job_country = job_country;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getGcpc() {
		return gcpc;
	}

	public void setGcpc(String gcpc) {
		this.gcpc = gcpc;
	}

	public String getPostingDate() {
		return postingDate;
	}

	public void setPostingDate(String postingDate) {
		this.postingDate = postingDate;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getCpc() {
		return cpc;
	}

	public void setCpc(String cpc) {
		this.cpc = cpc;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
