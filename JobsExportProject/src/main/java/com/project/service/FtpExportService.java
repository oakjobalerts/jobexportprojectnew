package com.project.service;

import com.project.model.json.ExportSourcesModel;

public interface FtpExportService {

	// void uploadFileOverFtpServer();

	String uploadSFTPFile(ExportSourcesModel model);

}
