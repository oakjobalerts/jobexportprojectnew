package com.project.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "job")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExportJujuModel implements Serializable {

    private static final long serialVersionUID = 1L;

    // TODO IMPORTANT : change setters methods to include cdata format

    @XmlAttribute
    String id;

    String employer;
    String title;
    String description;
    String postingdate;
    String joburl;
    String location;
    String salary;
    String type;
    String experience;
    String education;
    String jobsource;
    String jobsourceurl;
    String cpc;

    public String getCpc() {
        return cpc;
    }

    public void setCpc(String cpc) {
        this.cpc = "<![CDATA[" + cpc + " ]]>";
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = "<![CDATA[" + employer + " ]]>";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = "<![CDATA[" + title + " ]]>";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = "<![CDATA[" + description + " ]]>";
    }

    public String getPostingdate() {
        return postingdate;
    }

    public void setPostingdate(String postingdate) {
        this.postingdate = "<![CDATA[" + postingdate + " ]]>";
    }

    public String getJoburl() {
        return joburl;
    }

    public void setJoburl(String joburl) {
        this.joburl = "<![CDATA[" + joburl + " ]]>";
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = "<![CDATA[" + type + " ]]>";
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = "<![CDATA[" + education + " ]]>";
    }

    public String getJobsource() {
        return jobsource;
    }

    public void setJobsource(String jobsource) {
        this.jobsource = "<![CDATA[" + jobsource + " ]]>";
    }

    public String getJobsourceurl() {
        return jobsourceurl;
    }

    public void setJobsourceurl(String jobsourceurl) {
        this.jobsourceurl = "<![CDATA[" + jobsourceurl + " ]]>";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        // No cdata needed here
        this.id = id;
    }
}
