package com.project.model.json;

public class ExportSourceModelDB {
	String name;
	String a_id;
	String fileName;

	String xmlStartEelementName;
	String xmlPublisherElementName;
	String xmlPublisherurlElementName;
	String xmlLastBuildDateElementName;

	int jobExportLimit;
	boolean isActive;
	boolean cutCpcInHalf;

	String minCpc;

	String ftp_user;
	String ftp_pass;

	String includedTitles;
	String excludedTitles;
	String excludedSources;
	String includedSources;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getA_id() {
		return a_id;
	}

	public void setA_id(String a_id) {
		this.a_id = a_id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getXmlStartEelementName() {
		return xmlStartEelementName;
	}

	public void setXmlStartEelementName(String xmlStartEelementName) {
		this.xmlStartEelementName = xmlStartEelementName;
	}

	public String getXmlPublisherElementName() {
		return xmlPublisherElementName;
	}

	public void setXmlPublisherElementName(String xmlPublisherElementName) {
		this.xmlPublisherElementName = xmlPublisherElementName;
	}

	public String getXmlPublisherurlElementName() {
		return xmlPublisherurlElementName;
	}

	public void setXmlPublisherurlElementName(String xmlPublisherurlElementName) {
		this.xmlPublisherurlElementName = xmlPublisherurlElementName;
	}

	public String getXmlLastBuildDateElementName() {
		return xmlLastBuildDateElementName;
	}

	public void setXmlLastBuildDateElementName(String xmlLastBuildDateElementName) {
		this.xmlLastBuildDateElementName = xmlLastBuildDateElementName;
	}

	public int getJobExportLimit() {
		return jobExportLimit;
	}

	public void setJobExportLimit(int jobExportLimit) {
		this.jobExportLimit = jobExportLimit;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isCutCpcInHalf() {
		return cutCpcInHalf;
	}

	public void setCutCpcInHalf(boolean cutCpcInHalf) {
		this.cutCpcInHalf = cutCpcInHalf;
	}

	public String getMinCpc() {
		return minCpc;
	}

	public void setMinCpc(String minCpc) {
		this.minCpc = minCpc;
	}

	public String getFtp_user() {
		return ftp_user;
	}

	public void setFtp_user(String ftp_user) {
		this.ftp_user = ftp_user;
	}

	public String getFtp_pass() {
		return ftp_pass;
	}

	public void setFtp_pass(String ftp_pass) {
		this.ftp_pass = ftp_pass;
	}

	public String getIncludedTitles() {
		return includedTitles;
	}

	public void setIncludedTitles(String includedTitles) {
		this.includedTitles = includedTitles;
	}

	public String getExcludedTitles() {
		return excludedTitles;
	}

	public void setExcludedTitles(String excludedTitles) {
		this.excludedTitles = excludedTitles;
	}

	public String getExcludedSources() {
		return excludedSources;
	}

	public void setExcludedSources(String excludedSources) {
		this.excludedSources = excludedSources;
	}

	public String getIncludedSources() {
		return includedSources;
	}

	public void setIncludedSources(String includedSources) {
		this.includedSources = includedSources;
	}

}
