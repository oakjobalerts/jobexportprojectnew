package com.project.repository.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.codec.binary.Base64;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.opencsv.CSVWriter;
import com.project.adaptors.EscapeHandler;
import com.project.model.ExportGeneralModel;
import com.project.model.ExportJobCountModel;
import com.project.model.ExportJujuModel;
import com.project.model.ExportMacroModel;
import com.project.model.JobsModel;
import com.project.model.Url;
import com.project.model.UrlShortenrModel;
import com.project.model.ZippiaModel;
import com.project.model.json.ExportSourcesModel;
import com.project.model.json.SubIdModel;
import com.project.repository.JobsRepository;
import com.project.utility.Utility;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.xml.bind.marshaller.DataWriter;

@Repository
public class JobsRepositoryImpl implements JobsRepository {

	@Autowired
	SessionFactory sessionFactory;

	@Value("${xml.publisher.value}")
	String xmlPublisherValue;

	@Value("${xml.publisher.url.value}")
	String xmlPublisherUrlValue;

	@Value("${local.server.folder.path}")
	String folderPath;

	@Value("${jobs.export.table.name}")
	String exportTableName;

	@Value("${jobs.export.table.replacement.key.value}")
	String exportTableReplacementValue;

	@Value("${print.sys.out}")
	boolean isSysoutPrintingEnabled;

	@Override
	@Transactional
	public List<JobsModel> getJobsFromDbWithStartId(String query, int start, int limit) {

		try {

			query = query.replace(exportTableReplacementValue, exportTableName);

			Query sqlQuery = getSessionFactory().createSQLQuery(query + " limit " + start + ", " + limit)
					.setResultTransformer(Transformers.aliasToBean(JobsModel.class));

			@SuppressWarnings("unchecked")
			List<JobsModel> jobsModelList = sqlQuery.list();
			System.out.println("size of jobmodel list=" + jobsModelList.size());

			return jobsModelList;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public DataWriter writeStartElement(ExportSourcesModel exportSourcesModel) {

		// Fri, 10 Dec 2008 22:49:39 GMT
		DateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM YYYY hh:mm:ss z");
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));

		DataWriter dataWriter = null;

		try {
			PrintWriter writer = null;
			File file = new File(folderPath + exportSourcesModel.getFileName());
			writer = new PrintWriter(new FileWriter(file));
			dataWriter = new DataWriter(writer, "utf-8", new EscapeHandler());
			dataWriter.startDocument();
			dataWriter.characters("<" + exportSourcesModel.getXmlStartEelementName() + ">");
			dataWriter.characters("<" + exportSourcesModel.getXmlPublisherElementName() + ">" + xmlPublisherValue + "</" + exportSourcesModel.getXmlPublisherElementName() + ">");
			dataWriter.characters("<" + exportSourcesModel.getXmlPublisherurlElementName() + ">" + xmlPublisherUrlValue + "</" + exportSourcesModel.getXmlPublisherurlElementName() + ">");
			dataWriter
					.characters("<" + exportSourcesModel.getXmlLastBuildDateElementName() + ">" + dateFormatter.format(new Date()) + "</" + exportSourcesModel.getXmlLastBuildDateElementName() + ">");

			dataWriter.endDocument();
			dataWriter.flush();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dataWriter;
	}

	@Override
	public Marshaller createMarshaller(String source) {
		Marshaller marshaller = null;
		try {

			JAXBContext jaxbContext = JAXBContext.newInstance(ExportGeneralModel.class);

			if (source.toLowerCase().equals("juju")) {
				jaxbContext = JAXBContext.newInstance(ExportJujuModel.class);
			}

			if (source.toLowerCase().equals("zippia")) {
				jaxbContext = JAXBContext.newInstance(ZippiaModel.class);
			}
			if (source.toLowerCase().equals("macro") || source.toLowerCase().equals("jobtome")
					|| source.toLowerCase().equals("nexxt")) {
				jaxbContext = JAXBContext.newInstance(ExportMacroModel.class);

			}

			marshaller = jaxbContext.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return marshaller;
	}

	// Here we cut cpc in half and also if any error occurs we sent null cpc
	// back and hence leave the job altogether
	@Override
	public String cutCpcStringToHalf(String cpc, JobsModel model) {

		try {

			// here we check sources are having cpc greate that 1.2 $
			if (Float.parseFloat(model.getCpc()) > 1.2) {
				if (Utility.cpc_more_than_one_point_two_map.get(model.getSourceName() + "_" + model.getCpc()) == null) {
					Utility.cpc_more_than_one_point_two_map.put(model.getSourceName() + "_" + model.getCpc(), model.getCpc());
				}

			}

			float floatCpc = Float.parseFloat(cpc);
			floatCpc = floatCpc / 2;

			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			cpc = df.format(floatCpc);

			return cpc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String callUrlShortnerAPI(String url, WebResource webResource) {

		UrlShortenrModel urlShortenrModel = new UrlShortenrModel();
		urlShortenrModel.setUrl(url);

		String jsonUrl = new Gson().toJson(urlShortenrModel);

		try {
			ClientResponse clientResponse = webResource
					// .header("Authorization",
					// "Basic aWViYzluZmtodXoyOGR5dXM2YnF6NXlheHptOTY1d2M6")
					.type(MediaType.APPLICATION_JSON)
					.post(ClientResponse.class, jsonUrl);

			JsonObject domainJsonObject = new Gson().fromJson(clientResponse.getEntity(String.class), JsonObject.class);
			String response = domainJsonObject.get("shortUrl").toString();
			response = response.replace("\"", "");
			System.out.println("response"+response);

			return response;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String encryptUrl(Url url, String urlDomainParams, String urlEndParams, String aIdParamName) {

		Gson gson = new GsonBuilder().disableHtmlEscaping().create();

		String JobUrl = "";

		try {

			String jsonUrl = gson.toJson(url);
			byte[] encodedBytes = Base64.encodeBase64(jsonUrl.getBytes());
			String encodedurl = new String(encodedBytes);
			JobUrl = urlDomainParams + encodedurl + aIdParamName + urlEndParams;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		return JobUrl;
	}

	@Override
	public String updateSubIdBySourceInUrl(String source, String url, List<SubIdModel> subIdLIst) {

		for (SubIdModel subIdModel : subIdLIst) {

			try {

				if (source.toLowerCase().contains(subIdModel.getSource().toLowerCase())) {

					String repolacedUrl = url;
					repolacedUrl = repolacedUrl.replace(subIdModel.getRemoveSubString(), "");
					repolacedUrl = repolacedUrl + subIdModel.getSubId();

					return repolacedUrl;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return url;
	}

	public Session getSessionFactory() {
		return sessionFactory.getCurrentSession();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public CSVWriter writeCsvFileInLocal(ExportSourcesModel exportSourcesModel) {
		// TODO Auto-generated method stub
		CSVWriter csvWriter = null;
		try {
			csvWriter = new CSVWriter(new FileWriter(folderPath + exportSourcesModel.getName() + ".csv"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String[] firstRow = { "jobId",
				"title",
				"category",
				"city",
				"state",
				"zipcode",
				"source",
				"cpc",
				"jobType",
				"description",
				"company",
				"url",
				"postingdate" };
		csvWriter.writeNext(firstRow);

		return csvWriter;
	}

	@Override
	@Transactional
	public int getinitialCountFromDb(String query) {
		// TODO Auto-generated method stub
		query = query.replace(exportTableReplacementValue, exportTableName);

		System.out.println("query=" + query);
		Query sqlQuery = getSessionFactory().createSQLQuery(query)
				.setResultTransformer(Transformers.aliasToBean(ExportJobCountModel.class));

		@SuppressWarnings("unchecked")
		List<ExportJobCountModel> countModelList = sqlQuery.list();
		System.out.println("size of jobmodel list=" + countModelList.size());

		return countModelList.get(0).getCount().intValue();
	}

}
